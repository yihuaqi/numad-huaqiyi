package edu.neu.madcourse.huaqiyi.dabbletest;

import java.util.HashMap;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import edu.neu.madcourse.huaqiyi.R;

import edu.neu.madcourse.huaqiyi.dabbletest.Register.AsyncAddPlayer;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mhealth.api.KeyValueAPI;
import edu.neu.mobileClass.*;

public class MultiLogIn extends Activity {

	Button loginButton;
	Button createButton;
	Button quitButton;
	Button findButton;
	EditText userNameEditText;
	TextView loginText;
	String TAG = "Dabble multi test login";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.dabble_test_multi_login);
        
        //PhoneCheckAPI.doAuthorization(this);
        loginButton = (Button) findViewById(R.id.dabble_multi_login_loginButton);
        createButton  = (Button) findViewById(R.id.dabble_multi_login_createButton);
        findButton = (Button) findViewById(R.id.dabble_multi_login_findButton);
        quitButton = (Button) findViewById(R.id.dabble_multi_login_quitButton);
        userNameEditText = (EditText) findViewById(R.id.dabble_multi_login_usernameEditText);
        loginText = (TextView) findViewById(R.id.dabble_multi_login_usernameTextView);
        ButtonListener bl = new ButtonListener();
        loginButton.setOnClickListener(bl);
        createButton.setOnClickListener(bl);
        findButton.setOnClickListener(bl);
        quitButton.setOnClickListener(bl);
        if(!Constants.getPlayer().equals("")){
        	loginText.setText(Constants.getPlayer());
        }
    }	

    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {
			

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.dabble_multi_login_loginButton) {
				Constants.setPlayer( userNameEditText.getText().toString());
				loginText.setText("Logged In:"+Constants.USERNAME);
				asyncAddPlayer(Constants.getPlayer());
			} else if (id == R.id.dabble_multi_login_createButton) {
				i.setClass(MultiLogIn.this, MultiCreateGame.class);
				startActivity(i);
			} else if (id == R.id.dabble_multi_login_findButton) {
				i.setClass(MultiLogIn.this, MultiFindGame.class);
				startActivity(i);
			} else if (id == R.id.dabble_multi_login_quitButton) {
				finish();
			} else {
			}
			
		}
    }

	public void asyncAddPlayer(String name) {
		AsyncTask<String,String,String> addPlayerTask = new AsyncAddPlayer();
		
		addPlayerTask.execute(Constants.PLAYERLIST,name);
		
	}

	

    public class AsyncAddPlayer extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String players = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, arg0[0]);
    		String result;
    		if(players.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			result = arg0[1];
    		} else{
    			boolean alreadyRegistered=false;
    			String[] playerList = Constants.getPlayerList(players);
    			for(int i = 0; i < playerList.length; i++){
    				alreadyRegistered = (playerList[i].equals(arg0[1])) || alreadyRegistered;
    			}
    			if(!alreadyRegistered){

    				result = Constants.appendPlayer(players,arg0[1]);
    				
    			} else{
    				return null;
    			}
    		}
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, arg0[0],result);
    		Log.d(TAG,"KeyValueAPI put "+arg0[0]+" "+arg0[1]);
    		return null;
    	}
    }
}
