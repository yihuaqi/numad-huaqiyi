
package edu.neu.madcourse.huaqiyi.dabbletest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.Dabble;
import edu.neu.madcourse.huaqiyi.dabbletest.CreateAndFindGame.AsyncInvite;
import edu.neu.madcourse.huaqiyi.dabbletest.Register.AsyncGetPlayer;
import edu.neu.madcourse.huaqiyi.dabbletest.TestMainMenu.ButtonListener;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mobileClass.*;
import edu.neu.mhealth.api.*;



public class MultiCreateGame extends Activity{

	Button backButton;
	Button createButton;
	TextView opponentName;
	SimpleAdapter listItemAdapter;
	ListView list;
	String TAG = "Dabble Multi Create";

			
	ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();  
    @Override
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.dabble_test_multi_creategame);  

        
        backButton = (Button) findViewById(R.id.dabble_test_multi_create_backButton);
        createButton = (Button) findViewById(R.id.dabble_test_multi_create_createButton);
        opponentName = (TextView) findViewById(R.id.dabble_test_multi_create_opponentName);
        list = (ListView) findViewById(R.id.dabble_test_multi_create_ListView);
        ButtonListener bl = new ButtonListener();
        backButton.setOnClickListener(bl);
        createButton.setOnClickListener(bl);
        

        listItemAdapter = new SimpleAdapter(this, 
							                listItem,
							                R.layout.dabble_test_multi_creategamelistitem,
							                new String[] {Constants.PLAYERNAMEKEY},   
							                new int[] {R.id.dabble_test_multi_creategame_listItem_PlayerName});  
		
		list.setAdapter(listItemAdapter);
		list.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3){
				Log.d(TAG, "CLICK ON "+arg3);

				Constants.setOpponent(listItem.get((int)arg3).get(Constants.PLAYERNAMEKEY));
				opponentName.setText(Constants.getOpponent());
			}
		});
		asyncGetPlayer();
    }  
    private void asyncGetPlayer() {
		AsyncTask<String, String,String> getPlayerTask = new AsyncGetPlayer();
		getPlayerTask.execute(Constants.PLAYERLIST);
		
	}
    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.dabble_test_multi_create_backButton) {
				finish();
			} else if (id == R.id.dabble_test_multi_create_createButton) {
				//asyncAddHighScore(nameEditText.getText().toString(),scoreEditText.getText().toString());
				asyncInvitePlayer(Constants.getPlayer(),Constants.getOpponent());
				Log.d(TAG, "Play- Player:"+Constants.getPlayer()+" Opponent:"+Constants.getOpponent()+" GameId"+Constants.getGameId());
				i.setClass(MultiCreateGame.this, MultiGame.class);
				i.putExtra(Constants.HAS_BOARD, false);
				startActivity(i);
			} else {
			}
			
		}
    }    
    public class AsyncGetPlayer extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {

    		String result = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, arg0[0]);
    		Log.d(TAG,"KeyValueAPI get "+result);
    		return result;
    	}
    	
    	protected void  onPostExecute(String result){
    		try{
    			
    			if(result.equals(Constants.EXCEPTIONNOSUCHKEY)){
    				listItem.clear();
    			} else {
    				Log.d(TAG,"KeyValueAPI get "+result);
    				String[] resultRecord = Constants.getPlayerList(result);
    				for(int i = 0; i < resultRecord.length; i++){
    			        HashMap<String, String> map = new HashMap<String, String>();  
    			        if(!resultRecord[i].equals(Constants.getPlayer())){
    			        	map.put(Constants.PLAYERNAMEKEY, resultRecord[i]);  
    			        
    			        	listItem.add(map);  
    			        }
    				}
    		
    				listItemAdapter.notifyDataSetChanged();
    			}
    		} catch (Exception e){
    			Toast.makeText(getApplicationContext(), "FAIL TO GET PlayerList", Toast.LENGTH_SHORT).show();
    		}
    	}

    }
    
	void asyncInvitePlayer(String playerName, String opponentName){
		AsyncTask<String,String,String> invitePlayerTask = new AsyncInvite();
		String timeStamp = System.currentTimeMillis()+"";
		String gameName = Constants.makeGameName(timeStamp,playerName,opponentName);
		Constants.setGameId(gameName);
		invitePlayerTask.execute(playerName,opponentName,gameName);
	}
    
    public class AsyncInvite extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String playerName = arg0[0];
    		String opponentName = arg0[1];
    		String gameName = arg0[2];
    		String playerGames = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(playerName));
    		String playerGamesResult;
    		if(playerGames.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			playerGamesResult = gameName;
    			
    		} else {
    			 playerGamesResult = Constants.appendGame(playerGames,gameName);
    		}
    		Log.d(TAG,"PUT "+Constants.queryPlayerGameList(arg0[0])+" "+playerGamesResult);
    		String opponentGames = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(opponentName));
    		String opponentGamesResult;
    		if(opponentGames.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			opponentGamesResult = gameName;
    		} else {
    			opponentGamesResult = Constants.appendGame(opponentGames, gameName);
    		}
    		Log.d(TAG,"PUT "+Constants.queryPlayerGameList(arg0[1])+" "+opponentGamesResult);
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(playerName),playerGamesResult);
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(opponentName),opponentGamesResult);
    		
    		
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryGameStatus(),Constants.makePlayerTurn(playerName));
    		return null;
    	}
    	


    }

}
