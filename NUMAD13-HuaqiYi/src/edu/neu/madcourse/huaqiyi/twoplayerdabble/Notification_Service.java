package edu.neu.madcourse.huaqiyi.twoplayerdabble;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;

import android.content.Context;
import android.content.Intent;

import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import edu.neu.madcourse.huaqiyi.R;
import edu.neu.mhealth.api.KeyValueAPI;

public class Notification_Service extends Service{
	String TAG = "TWO PLAYER NOTIFICATION";
	
	public void onCreate(){
		Log.d(TAG,"Service created");
		new Thread(new getNotification()).start();
		
	}
	
	public int onStartCommand(Intent intent,int flags,int startId){
		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	class getNotification implements Runnable {

		@Override
		public void run() {
			
			
			while(true){
				try{
					Thread.sleep(1000);
					String queryNID = Constants.queryNotificationId(Constants.getPlayer());
					String notifications = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, queryNID);
					Log.d(TAG, " Get "+notifications+" by key "+queryNID);
					if(notifications.equals(Constants.EMPTYRECORD)||notifications.equals(Constants.EXCEPTIONNOSUCHKEY)){
						
					} else {
						String[] notificationList = Constants.getNotificationList(notifications);
						
						for(int i = 0; i < notificationList.length; i++){
							String message = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, notificationList[i]);
							Log.d(TAG, " Get "+message+" by key "+notificationList[i]);
							if(!message.equals(Constants.EMPTYRECORD)&&!notifications.equals(Constants.EXCEPTIONNOSUCHKEY)){
								handleNotification(notificationList[i],message);
								KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, queryNID,Constants.EMPTYRECORD);
								Log.d(TAG, " PUT "+Constants.EMPTYRECORD+" by key "+queryNID);
							}
						}
					}
					
				} catch(Exception e){
					
				}
			}
			
		}

		private void handleNotification(String nId, String message) {
			String Notification[] = Constants.getNotification(nId, message);
			showCustomizeNotification(Notification[0],Notification[1],Notification[2],nId);
			
		}

		private void showCustomizeNotification(String sender, String time,
				String message,String nId) {
			String[] messages = Constants.getMessage(message);
			String title = "Dabble";
			int icon = R.drawable.icon_monokuma_mdpi;
			NotificationCompat.Builder mBuilder = 
					new NotificationCompat.Builder(Notification_Service.this)
					.setSmallIcon(icon)
					.setContentTitle(title)
					.setContentText(messages[0]);
			Intent resultIntent = new Intent(Notification_Service.this,MultiGame.class);
			resultIntent.putExtra(Constants.HAS_BOARD, true);
			resultIntent.putExtra(Constants.GAME_ID, Constants.makeGameName(messages[3], messages[1], messages[2]));
			Log.d(TAG, "showCustomizeNotification: putExtra: "+Constants.makeGameName(messages[3], messages[1], messages[2]));
			
			
			PendingIntent resultPendingIntent = PendingIntent.getActivity(Notification_Service.this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			mBuilder.setContentIntent(resultPendingIntent);
			mBuilder.setAutoCancel(true);
			NotificationManager mNM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNM.notify(1,mBuilder.build());
			
			//mNM.cancel(1);
			
			
		}
		
	}


}
