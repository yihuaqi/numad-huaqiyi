package edu.neu.madcourse.huaqiyi;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import edu.neu.madcourse.huaqiyi.dabble.Dabble;
import edu.neu.madcourse.huaqiyi.dabbletest.SingleGame;
import edu.neu.madcourse.huaqiyi.dabbletest.TestMainMenu;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.madcourse.huaqiyi.twoplayerdabble.TwoPlayerDabble;
import edu.neu.mobileClass.*;
import edu.neu.madcourse.xipengwang.*;
public class MainActivity extends Activity {

	Button aboutButton;
	Button generateErrorButton;
	Button sudokuButton;
	Button dictionaryButton;
	Button quitButton;
	Button dabbleButton;
	Button dabbleTestButton;
	Button twoPlayerDabbleButton;
	Button trickestPartButton;
	String TAG = "Dabble";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_name);
        setContentView(R.layout.activity_main);
        
        PhoneCheckAPI.doAuthorization(this);
        aboutButton = (Button) findViewById(R.id.aboutButton);
        generateErrorButton = (Button)findViewById(R.id.generateErrorButton);
        sudokuButton = (Button)findViewById(R.id.sudokuButton);
        dictionaryButton = (Button) findViewById(R.id.dictionaryButton);
        quitButton = (Button) findViewById(R.id.quitButton);
        dabbleButton = (Button) findViewById(R.id.dabbleButton);
        dabbleTestButton = (Button) findViewById(R.id.dabbleTestButton);
        twoPlayerDabbleButton = (Button) findViewById(R.id.twoPlayerDabbleButton);
        trickestPartButton = (Button) findViewById(R.id.trickestPartButton);
        ButtonListener bl = new ButtonListener();
        aboutButton.setOnClickListener(bl);
        generateErrorButton.setOnClickListener(bl);
        sudokuButton.setOnClickListener(bl);
        dictionaryButton.setOnClickListener(bl); 
        quitButton.setOnClickListener(bl);
        dabbleButton.setOnClickListener(bl);
        dabbleTestButton.setOnClickListener(bl);    
        twoPlayerDabbleButton.setOnClickListener(bl);
        trickestPartButton.setOnClickListener(bl);
    }	

    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {
			// TODO Auto-generated method stub

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.aboutButton) {
				i.setClass(MainActivity.this, AboutActivity.class);
				startActivity(i);
			} else if (id == R.id.generateErrorButton) {
				setContentView(0);
			} else if (id == R.id.sudokuButton) {
				i.setClass(MainActivity.this, Sudoku.class);
				startActivity(i);
			} else if (id == R.id.dictionaryButton) {
				i.setClass(MainActivity.this, Dictionary.class);
				startActivity(i);
				Log.d(TAG, "CLICK ON DICTIONARY BUTTON");
			} else if (id == R.id.quitButton) {
				finish();
			} else if (id == R.id.dabbleButton) {
				i.setClass(MainActivity.this, Dabble.class);
				startActivity(i);
				Log.d(TAG, "CLICK ON DABBLE BUTTON");
			} else if (id == R.id.dabbleTestButton) {
				i.setClass(MainActivity.this,TestMainMenu.class);
				startActivity(i);
			} else if (id == R.id.twoPlayerDabbleButton) {
				if(isNetworkConnected(MainActivity.this)){
					i.setClass(MainActivity.this, TwoPlayerDabble.class);
					startActivity(i);
				} else {
					Toast.makeText(MainActivity.this, "Network is unavailable", Toast.LENGTH_SHORT).show();
					
				}
			} else if (id == R.id.trickestPartButton) {
				 Intent intent = new Intent("android.intent.action.MAIN");
				 intent.setComponent(ComponentName.unflattenFromString("edu.neu.madcourse.xipengwang/edu.neu.madcourse.xipengwang.MainActivity"));
				 intent.addCategory("android.intent.category.LAUNCHER");
				 startActivity(intent);

			} else {
			}
			
		}
    	
    }
    
    public boolean isNetworkConnected(Context context){
    	if(context !=null){
    		ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
    		if(mNetworkInfo!=null){
    			return mNetworkInfo.isAvailable();
    		}
    	}
    	return false;
    }

    
}
