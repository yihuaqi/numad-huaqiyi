package edu.neu.madcourse.huaqiyi.dabbletest;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import edu.neu.madcourse.huaqiyi.R;


import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mhealth.api.KeyValueAPI;
import edu.neu.mobileClass.*;

public class TestMainMenu extends Activity {

	Button highScoreButton;
	Button registerButton;
	Button quitButton;
	Button createFindGameButton;
	Button clearAllButton;
	Button dabbleButton;
	Button notificationButton;
	Button acknowledgementButton;
	String TAG = "Dabble";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.dabble_test);
        
        //PhoneCheckAPI.doAuthorization(this);
        highScoreButton = (Button) findViewById(R.id.dabbleTestMainHighScoreButton);
        registerButton = (Button) findViewById(R.id.dabbleTestMainRegisterButton);
        quitButton = (Button) findViewById(R.id.dabbleTestMainQuitButton);
        createFindGameButton = (Button) findViewById(R.id.dabbleTestMainCreateAndFindButton);
        clearAllButton = (Button) findViewById(R.id.dabbleTestMainClearAllButton);
        dabbleButton = (Button) findViewById(R.id.dabbleTestMainDabbleButton);
        notificationButton = (Button) findViewById(R.id.dabbleTestMainNotificationButton);
        acknowledgementButton = (Button) findViewById(R.id.dabbleTestMainAcknowledgeButton);
        ButtonListener bl = new ButtonListener();
        highScoreButton.setOnClickListener(bl);
        registerButton.setOnClickListener(bl);
        quitButton.setOnClickListener(bl);
        createFindGameButton.setOnClickListener(bl);
        clearAllButton.setOnClickListener(bl);        
        dabbleButton.setOnClickListener(bl);
        notificationButton.setOnClickListener(bl);
        acknowledgementButton.setOnClickListener(bl);
        
        Intent i = new Intent();
        i.setClass(TestMainMenu.this, Notification_Service.class);
        //startService(i);
    }	

    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {
			// TODO Auto-generated method stub

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.dabbleTestMainHighScoreButton) {
				i.setClass(TestMainMenu.this, HighScore.class);
				startActivity(i);
			} else if (id == R.id.dabbleTestMainQuitButton) {
				finish();
			} else if (id == R.id.dabbleTestMainRegisterButton) {
				i.setClass(TestMainMenu.this, Register.class);
				startActivity(i);
			} else if (id == R.id.dabbleTestMainCreateAndFindButton) {
				i.setClass(TestMainMenu.this, CreateAndFindGame.class);
				startActivity(i);
			} else if (id == R.id.dabbleTestMainClearAllButton) {
				new AsyncClearAll().execute();
			} else if (id == R.id.dabbleTestMainNotificationButton) {
				i.setClass(TestMainMenu.this, NotificationTest.class);
				startActivity(i);
			} else if (id == R.id.dabbleTestMainDabbleButton) {
				i.setClass(TestMainMenu.this, Dabble.class);
				startActivity(i);
			} else if (id == R.id.dabbleTestMainAcknowledgeButton) {
				i.setClass(TestMainMenu.this, Acknowledgements.class);
				startActivity(i);
			} else {
			}
			
		}
	    public class AsyncClearAll extends AsyncTask<String, String,String>{

	    	@Override
	    	protected String doInBackground(String... arg0) {

	    		KeyValueAPI.clear(Constants.TEAMNAME, Constants.PASSWORD);
	    		return null;
	    	}
    	
	    }
    }
    
    
}
