/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
***/
package edu.neu.madcourse.huaqiyi.dabbletest;

import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.R.array;
import edu.neu.madcourse.huaqiyi.R.id;
import edu.neu.madcourse.huaqiyi.R.layout;
import edu.neu.madcourse.huaqiyi.R.menu;
import edu.neu.madcourse.huaqiyi.R.string;
import edu.neu.madcourse.huaqiyi.dabble.Prefs;
import edu.neu.madcourse.huaqiyi.dabbletest.TestMainMenu.ButtonListener.AsyncClearAll;
import edu.neu.mhealth.api.KeyValueAPI;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

public class Dabble extends Activity implements OnClickListener {
   private static final String TAG = "Dabble";
   
   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Log.d(TAG,"Dabble onCreate");
      setContentView(R.layout.dabble_test_main);

      // Set up click listeners for all the buttons
      View singleButton = findViewById(R.id.dabbleTestSingleGameButton);
      singleButton.setOnClickListener(this);
      View multiButton = findViewById(R.id.dabbleTestMultipleGameButton);
      multiButton.setOnClickListener(this);
      View clearAllButton = findViewById(R.id.dabbleTestClearAllButton);
      clearAllButton.setOnClickListener(this);
      View highScoreButton = findViewById(R.id.dabbleTestHighScoreButton);
      highScoreButton.setOnClickListener(this);
      View quitButton = findViewById(R.id.dabbleTestQuitButton);
      quitButton.setOnClickListener(this);
   }

   @Override
   protected void onResume() {
      super.onResume();
      //TODO get music;
      Music.play(this, R.raw.dabble_background);
      
   }

   @Override
   protected void onPause() {
      super.onPause();
      Music.stop(this);
   }

   public void onClick(View v) {
	   Intent i;
      int id = v.getId();
	if (id == R.id.dabbleTestSingleGameButton) {
		i = new Intent(this, SingleGame.class);
		startActivity(i);
	} else if (id == R.id.dabbleTestMultipleGameButton) {
		i = new Intent(this, MultiLogIn.class);
		startActivity(i);
	} else if (id == R.id.dabbleTestClearAllButton) {
		new AsyncClearAll().execute();
	} else if (id == R.id.dabbleTestHighScoreButton) {
	} else if (id == R.id.dabbleTestQuitButton) {
		finish();
	}
   }
   public class AsyncClearAll extends AsyncTask<String, String,String>{

   	@Override
   	protected String doInBackground(String... arg0) {

   		KeyValueAPI.clear(Constants.TEAMNAME, Constants.PASSWORD);
   		return null;
   	}
	
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      super.onCreateOptionsMenu(menu);
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.dabble_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int itemId = item.getItemId();
	if (itemId == R.id.dabble_settings) {
		startActivity(new Intent(this, Prefs.class));
		return true;
      // More items go here (if any) ...
	}
      return false;
   }

  


   /** Start a new game with the given difficulty level */
   private void startGame(int i) {
      Log.d(TAG, "clicked on " + i);
      Intent intent = new Intent(this, MultiGame.class);
      //intent.putExtra(Game.KEY_DIFFICULTY, i);
      startActivity(intent);
   }
}