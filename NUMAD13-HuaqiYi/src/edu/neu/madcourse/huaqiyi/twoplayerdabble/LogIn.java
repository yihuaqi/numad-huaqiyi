package edu.neu.madcourse.huaqiyi.twoplayerdabble;

import edu.neu.madcourse.huaqiyi.R;



import edu.neu.mhealth.api.KeyValueAPI;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class LogIn extends Activity implements OnClickListener{
	String TAG = "Two Player Dabble";
	TextView statusTextView;
	Button backButton;
	Button loginButton;
	EditText userNameEditText;
	EditText passwordEditText;
	final int USERNAMENOTEXIST = 0;
	final int LOGINSUCCESS = 1;
	final int WRONGPASSWORD = 2;
	
	public void onCreate(Bundle savedInstanceState) {
	      super.onCreate(savedInstanceState);
	      
	      setContentView(R.layout.two_player_dabble_login);	
	      backButton = (Button) findViewById(R.id.two_player_dabble_login_back);
	      statusTextView = (TextView) findViewById(R.id.two_player_dabble_login_status_Text);
	      loginButton = (Button) findViewById(R.id.two_player_dabble_login_login);
	      userNameEditText = (EditText) findViewById(R.id.two_player_dabble_login_username_editText);
	      passwordEditText = (EditText) findViewById(R.id.two_player_dabble_login_password_editText);
	      backButton.setOnClickListener(this);
	      loginButton.setOnClickListener(this);
	     
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.two_player_dabble_login_back) {
			finish();
		} else if (id == R.id.two_player_dabble_login_login) {
			String username = userNameEditText.getText().toString();
			String password = passwordEditText.getText().toString();
			AsyncTask<String,Integer,Void> loginTask = new AsyncLogIn();
			loginTask.execute(username,password);
		}
		
	}
	
    public class AsyncLogIn extends AsyncTask<String, Integer,Void>{

    	@Override
    	protected Void doInBackground(String... arg0) {
    		String username = arg0[0];
    		String password = arg0[1];
    		String passwordRecord = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryUserPassword(username));
    		if(!passwordRecord.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			if(password.equals(passwordRecord)){
    				Constants.setPlayer(username);
    				Constants.setLogInStatus(true);
    				publishProgress(LOGINSUCCESS);
    				Log.d(TAG,"Login sucessful as "+username);
    			} else {
    				publishProgress(WRONGPASSWORD);
    				Log.d(TAG,"Wrong password by "+username);
    			}
    		} else {
    			publishProgress(USERNAMENOTEXIST);
    			Log.d(TAG,"Username not exist: "+username);
    		}
    		
			return null;
    		
    	}
    	protected void onProgressUpdate(Integer... progress) {
    		switch(progress[0]){
    		case LOGINSUCCESS:
    			statusTextView.setText("Logged In As "+Constants.getPlayer());
    			finish();
    			break;
    		case WRONGPASSWORD:
    			statusTextView.setText("Wrong Password");
    			break;
    		case USERNAMENOTEXIST:
    			statusTextView.setText("Username Not Exist");
    			break;
    			
    		}
    	}
    	


    }

}
