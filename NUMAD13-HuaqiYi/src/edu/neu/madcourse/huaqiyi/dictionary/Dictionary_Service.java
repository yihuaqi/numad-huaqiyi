package edu.neu.madcourse.huaqiyi.dictionary;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import android.R.integer;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import edu.neu.madcourse.huaqiyi.R;

public class Dictionary_Service extends Service{

	private final IBinder dBinder = new DictionaryBinder();
	private HashMap<String, String> dictionaryHashMap = new HashMap<String, String>(800000);
	

	
	HashMap<String, String> wordlist = new HashMap<String,String>();
	
	
	public class DictionaryBinder extends Binder {
		Dictionary_Service getService(){
			return Dictionary_Service.this;
		}
	}
	
	public void onCreate(){
		//new MyThread().start();
		
	}
	
	public int onStartCommand(Intent intent,int flags,int startId){
		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return dBinder;
	}
	
	public boolean containsWord(String word){
		boolean result = dictionaryHashMap.containsKey(word);
		return result;

	}
	
	public boolean inWordlist(String word){
		return wordlist.containsKey(word);
	}
	
	public void putWordlist(String word){
		wordlist.put(word, "");
	}
	
	public void clearWordlist(){
		wordlist.clear();
	}

    public boolean inDictionary(String word){
    	boolean result = false;
    	String filename = word.length()+"/" +word.substring(0,2);
    	
    	try{
    		InputStream is = getAssets().open(filename);
    		InputStreamReader isReader = new InputStreamReader(is);
    		BufferedReader bReader = new BufferedReader(isReader);
    		String temp = null;
    		while((temp = bReader.readLine())!=null){
    			if(temp.equals(word)){
    				return true;
    			}
    		}
    		
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	return result;
    }
    
}
