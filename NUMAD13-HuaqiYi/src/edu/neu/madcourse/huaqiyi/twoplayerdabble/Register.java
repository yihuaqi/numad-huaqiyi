package edu.neu.madcourse.huaqiyi.twoplayerdabble;

import edu.neu.madcourse.huaqiyi.R;


import edu.neu.mhealth.api.KeyValueAPI;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Register extends Activity implements OnClickListener{
	String TAG = "Two Player Dabble";
	TextView statusTextView;
	Button backButton;
	Button registerButton;
	EditText userNameEditText;
	EditText passwordEditText;
	EditText confirmPasswordEditText;
	
	
	public void onCreate(Bundle savedInstanceState) {
	      super.onCreate(savedInstanceState);
	      Log.d(TAG,"Dabble onCreate");
	      setContentView(R.layout.two_player_dabble_register);
	      backButton = (Button) findViewById(R.id.two_player_dabble_register_back);
	      statusTextView = (TextView) findViewById(R.id.two_player_dabble_register_status_Text);
	      registerButton = (Button) findViewById(R.id.two_player_dabble_register_register);
	      userNameEditText = (EditText) findViewById(R.id.two_player_dabble_register_username_editText);
	      passwordEditText = (EditText) findViewById(R.id.two_player_dabble_register_password_editText);
	      confirmPasswordEditText = (EditText) findViewById(R.id.two_player_dabble_register_confirm_password_editText);
	      backButton.setOnClickListener(this);
	      registerButton.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.two_player_dabble_register_back) {
			finish();
		} else if (id == R.id.two_player_dabble_register_register) {
			String username = userNameEditText.getText().toString();
			String password = passwordEditText.getText().toString();
			String confirmPassword = confirmPasswordEditText.getText().toString();
			AsyncRegister registerTask = new AsyncRegister();
			registerTask.execute(username,password,confirmPassword);
		}
		
	}
	
	public class AsyncRegister extends AsyncTask<String, Integer,Void>{

    	private final int USERNAMEALREADYEXIST = 0;
		private final int REGISTERSUCCESS = 1;
		private final int PASSWORDINCONSISTENT = 2;
		@Override
    	protected Void doInBackground(String... arg0) {
    		String username = arg0[0];
    		String password = arg0[1];
    		String confirmedPassword = arg0[2];
    		String passwordRecord = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryUserPassword(username));
    		if(!passwordRecord.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			publishProgress(USERNAMEALREADYEXIST);
    			Log.d(TAG,"Register Username Exists: "+username+" "+password);
    		} else {
    			if(password.equals(confirmedPassword)){

    				registerPlayer(username,password);
    	    		addToPlayerList(username);
    	    		Log.d(TAG,"Register: "+username+" "+password);
    	    		
    			} else {
    				publishProgress(PASSWORDINCONSISTENT);
    				Log.d(TAG,"Inconsistent password ");
    			}
    		}
    		
			return null;
    		
    	}
    	private void registerPlayer(String username, String password) {
			KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryUserPassword(username),password);
			publishProgress(REGISTERSUCCESS);
			Constants.setPlayer(username);
			Constants.setLogInStatus(true);
			
		}
		private void addToPlayerList(String username) {
    		String players = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.PLAYERLIST);
    		String result;
    		
    		if(players.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			result = username;
    		} else{
    			result = Constants.appendPlayer(players, username);
    		}
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.PLAYERLIST,result);
			
		}
    	
    	
    	
		protected void onProgressUpdate(Integer... progress) {
    		switch(progress[0]){
    		case USERNAMEALREADYEXIST:
    			statusTextView.setText("This username has already existed");
    			break;
    		case REGISTERSUCCESS:
    			statusTextView.setText("Successful Register!");
    			finish();
    			break;
    		case PASSWORDINCONSISTENT:
    			statusTextView.setText("Inconsistent Password");
    			break;
    			
    		}
    	}
    	


    }


}
