package edu.neu.madcourse.huaqiyi.dabbletest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.Prefs;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SingleGame extends Activity {
	
	   private static final String TAG = "Dabble Single Test";
	   private ArrayList<Letter> letters = new ArrayList<Letter>();
	   public String solution;
	   public String boardWord;
	   public SoundPool soundPool;
	   public int DABBLE_SELECT;
	   public int DABBLE_VALID;
	   public int DABBLE_ALERT;
	   public int DABBLE_WIN;
	   public int DABBLE_LOSE;
	   static int numRoot[] = new int[]{45,87,133,182};
	   Button pauseButton;
	   Button hintButton;
	   Button quitButton;
	   TextView scoreText;
	   TextView timeText;
	   
	   
	   private boolean destroyTimer = false;
	   float font_size;
	   Paint selectedPaint;
	   Paint unSelectedPaint;
	   Paint validPaint;
	   Paint menuPaint;
	   Paint hintPaint;
	   Paint timerPaint;
	   Paint timerAlertPaint;
	   Paint background;
	   private  int gameStatus;
	   public final int GAME_PLAY = 1;
	   public final int GAME_PAUSE = 2;
	   public final int GAME_WIN = 3;
	   public final int GAME_LOSE =4;
	   public final int GAME_ALEART = 5;
	   private final int GAME_TIME = 120000;
	   private final int GAME_TIMEALERT = 30000;
	   public boolean isPlayingAlert = false;
	   private int timeLeft;
	   private int score;
	   boolean isPlayingLose = false;
	   public boolean allowToPlayAlert = false;
	   int alertStreamID=0;
	   boolean showHint = false;
	   RelativeLayout[] tiles = new RelativeLayout[18];
	   TextView[] tilesLetter = new TextView[18];
	   TextView[] tilesScore = new TextView[18];
	   int selectedTileIndex;
	   final int NOTILEISSELECTED = -1;
	   String playerStatus="";
	   String opponentStatus="";
	   TextView playerStatusText;
	   TextView opponentStatusText;
	   ScrollView playerStatusScroll;
	   ScrollView opponentStatusScroll;
	private boolean useAlertPaint = false;

	   protected void onCreate(Bundle savedInstanceState) {
		      super.onCreate(savedInstanceState);
		      Log.d(TAG, "Game onCreate");
		      iniGame();
		      timeLeft = GAME_TIME;
		      score=0;

		      allowToPlayAlert = true;
		      selectedTileIndex = NOTILEISSELECTED;
		      soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC,5);
		      //TODO find music
		      DABBLE_SELECT = soundPool.load(this,R.raw.dabble_select, 1);
		      DABBLE_VALID = soundPool.load(this,R.raw.dabble_valid, 1);
		      DABBLE_ALERT = soundPool.load(this,R.raw.dabble_alert, 1);
		      DABBLE_WIN = soundPool.load(this,R.raw.dabble_win, 1);
		      DABBLE_LOSE = soundPool.load(this,R.raw.dabble_lose, 1);
		      
		      
		      setContentView(R.layout.dabble_test_single);
		      
		      assignValue();
		      
		      new AsyncTimer().execute();
		      setGameStatus(GAME_PLAY);
		      iniLetters();
		      
		      
		      
		      // ...
		      // If the activity is restarted, do a continue next time
		      //getIntent().putExtra(KEY_DIFFICULTY, DIFFICULTY_CONTINUE);
		      

		      

		   }
	   private void assignValue() {
		int tileId = R.id.singleTile1;
		int letterId = R.id.singleLetter1;
		int scoreId = R.id.singleScore1;
		TileListener tl = new TileListener();
		ButtonListener bl = new ButtonListener();
		for(int i = 0; i < 18; i++){
				tiles[i] = (RelativeLayout) findViewById(tileId+3*i);
				tiles[i].setOnClickListener(tl);
				tilesLetter[i] = (TextView) findViewById(letterId+3*i);
				tilesScore[i] = (TextView) findViewById(scoreId+3*i);
				
				
			}
		scoreText = (TextView) findViewById(R.id.dabble_test_single_score);
		timeText = (TextView) findViewById(R.id.dabble_test_single_time);
		pauseButton = (Button)findViewById(R.id.singlePauseButton);
		pauseButton.setOnClickListener(bl);
		hintButton = (Button)findViewById(R.id.singleHintButton);
		hintButton.setOnClickListener(bl);
		quitButton = (Button)findViewById(R.id.singleQuitButton);
		quitButton.setOnClickListener(bl);
		playerStatusText = (TextView) findViewById(R.id.singleYourStatusText);
		//playerStatusText.setMovementMethod(new ScrollingMovementMethod());
		opponentStatusText = (TextView) findViewById(R.id.singleOpponentStatusText);
		//opponentStatusText.setMovementMethod(new ScrollingMovementMethod());
		playerStatusScroll = (ScrollView) findViewById(R.id.singleYourStatusScroll);
		opponentStatusScroll = (ScrollView) findViewById(R.id.singleOpponentStatusScroll);
		
		
	   }
	   
	   class TileListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			int tileId = R.id.singleTile1;
			int viewId = v.getId();
			int index = (viewId - tileId)/3;
			if(selectedTileIndex == NOTILEISSELECTED){
				selectedTileIndex = index;
				letters.get(index).setSelected();
			} else {
				if(index == selectedTileIndex){
					letters.get(index).setUnSelected();
					selectedTileIndex = NOTILEISSELECTED;
				} else {
					swapLetter(letters.get(index),letters.get(selectedTileIndex));
					checkValidWords();
				}
				
			}
			
		}

		   
	   }
		private void swapLetter(Letter firstL, Letter secondL) {
			//firstL.setUnSelected();
			//secondL.setUnSelected();
			   String firstScore = firstL.getScore();
			   String secondScore = secondL.getScore();
			   String firstLetter = firstL.getLetter();
			   String secondLetter = secondL.getLetter();
			   
			   firstL.setLetter(secondLetter);
			   firstL.setScore(secondScore);
			   
			   secondL.setLetter(firstLetter);
			   secondL.setScore(firstScore);
			   firstL.setUnSelected();
			   secondL.setUnSelected();
			   selectedTileIndex = NOTILEISSELECTED;
			   addPlayerMessage(" Swapped "+firstLetter+" with "+secondLetter+"\n");


			   
			
		}
	   class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			int tileId = R.id.singleTile1;
			int viewId = v.getId();
			int id = v.getId();
			if (id == R.id.singlePauseButton) {
				switch(gameStatus){
					case GAME_PLAY:
						setGameStatus(GAME_PAUSE);
						pauseButton.setText("Continue");
						allowToPlayAlert = false;
						for(Iterator<Letter> i = letters.iterator(); i.hasNext(); ){
							i.next().setInvisible(true);
						}
						break;
					case GAME_PAUSE:
						setGameStatus(GAME_PLAY);
						pauseButton.setText("Pause");
						allowToPlayAlert = true;
						for(Iterator<Letter> i = letters.iterator(); i.hasNext();){
							i.next().setInvisible(false);
						}
						break;
					case GAME_WIN:
					case GAME_LOSE:
						
						restartGame();
						break;
					}
			} else if (id == R.id.singleHintButton) {
				Log.d(TAG, "Clikc Hint Button: "+showHint);
				setShowHint(!showHint);
			} else if (id == R.id.singleQuitButton) {
				destroyTimer=true;
				soundPool.stop(alertStreamID);
				isPlayingAlert = false;
				allowToPlayAlert =false;
				Quit();
			}
			
		}
		   
	   }
	   
	   private void checkValidWords() {
		   String firstRow="";
			String secondRow="";
			String thirdRow="";
			String fourthRow="";
			for(int i = 0 ; i < 3 ; i++){
				firstRow += letters.get(i).getLetter();
				
			}
			Log.d(TAG, "First word is"+firstRow);
			
			for(int i = 3 ; i < 7 ; i++){
				secondRow += letters.get(i).getLetter();
				
			}
			Log.d(TAG, "First word is"+secondRow);
			
			for(int i = 7 ; i < 12 ; i++){
				thirdRow += letters.get(i).getLetter();
				
			}
			Log.d(TAG, "First word is"+thirdRow);
			
			for(int i = 12 ; i < 18 ; i++){
				fourthRow += letters.get(i).getLetter();
				
			}
			Log.d(TAG, "First word is"+fourthRow);
			boolean[] isValid = new boolean[]{false,false,false,false};
			isValid[0] = inDictionary(firstRow.toLowerCase());
			isValid[1] = inDictionary(secondRow.toLowerCase());
			isValid[2] = inDictionary(thirdRow.toLowerCase());
			isValid[3] = inDictionary(fourthRow.toLowerCase());
			if(isValid[0]){
				addPlayerMessage(" Spelt a valid word "+firstRow+"!"+"\n");
			}
			if(isValid[1]){
				addPlayerMessage(": Spelt a valid word "+secondRow+"!"+"\n");
			}
			if(isValid[2]){
				addPlayerMessage(": Spelt a valid word "+thirdRow+"!"+"\n");
			}
			if(isValid[3]){
				addPlayerMessage(": Spelt a valid word "+fourthRow+"!"+"\n");
			}
			if(isValid[0] || isValid[1] || isValid[2] || isValid[3]){
				soundPool.play(DABBLE_VALID, 1.0f, 1.0f, 0, 0, 1);
			}
			for(int i = 0 ; i < 3 ; i++){
				
				letters.get(i).setValid(isValid[0]);
				

			}
		
			
			

			for(int i = 3 ; i < 7 ; i++){
				letters.get(i).setValid(isValid[1]);
				//Log.d(TAG, secondRow+" First word is Valid");


			}
			
			
			

			for(int i = 7 ; i < 12 ; i++){
				letters.get(i).setValid(isValid[2]);
				//Log.d(TAG, thirdRow+" Third word is valid");

			}
			
			
			

			for(int i = 12 ; i < 18 ; i++){
				letters.get(i).setValid(isValid[3]);
				//Log.d(TAG, fourthRow + " Fourth word is valid");


			}
			score = 0;
			for(int i = 0; i < 4; i++){
				if(isValid[i]){
					score += getScoreInRow(i);
				}
			}
			checkWin(isValid);
		}
	   
		private void checkWin(boolean[] isValid) {
			if(isValid[0] && isValid[1] && isValid[2] && isValid[3] ){
				setGameStatus(GAME_WIN);
				score+= timeLeft/1000;
				Music.stop(this);
				soundPool.play(DABBLE_WIN, 1.0f, 1.0f, 0, 0, 1);
				soundPool.stop(alertStreamID);
			}
			
		}
		

	
	private int getScoreInRow(int i) {
		int result = 0;
		switch(i){
		case 0:
			
			for(int j = 0; j < 3; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 3;
		case 1:

			for(int j = 3; j < 7; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 4;
			
		case 2:

			for(int j = 7; j < 12; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 5;
			
		case 3:

			for(int j = 12; j < 18; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 6;
			
		}
		return 0;
	}
	private String getScoreForLetter(char charAt) {
		switch (charAt){
		case 'a':
		case 'e':
		case 'i':
		case 'l':
		case 'n':
		case 'o':
		case 'r':
		case 's':
		case 't':
		case 'u':
			return "3";
		case 'd':
		case 'g':
			return "6";
		case 'b':
		case 'c':
		case 'm':
		case 'p':
			return "9";
		case 'f':
		case 'h':
		case 'v':
		case 'w':
		case 'y':
			return "12";
		case 'k':
			return "15";
		case 'j':
		case 'x':
			return "24";
		case 'q':
		case 'z':
			return "30";
		}
		return "ERROR";
		
	}
	void iniGame(){
		   Log.d(TAG,"iniGame");
		   
		   solution = getWord(3)+getWord(4)+getWord(5)+getWord(6);
		   Log.d(TAG,"Get Solution:" + solution);
		   boardWord = randomizeWord(solution);
		   Log.d(TAG,"Get boardWord:"+boardWord);

		
	   }
	   
	   @Override
	   public boolean onCreateOptionsMenu(Menu menu) {
	      super.onCreateOptionsMenu(menu);
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.dabble_menu, menu);
	      return true;
	   }

	   @Override
	   public boolean onOptionsItemSelected(MenuItem item) {
	      int itemId = item.getItemId();
		if (itemId == R.id.dabble_settings) {
			startActivity(new Intent(this, Prefs.class));
			return true;
	      // More items go here (if any) ...
		}
	      return false;
	   }
	   protected void onResume(){
		   super.onResume();
		   //TODO add music file.
		   Music.play(this, R.raw.dabble_background);
	   }
	   protected void onPause(){
		   super.onPause();
		   if(getGameState()==GAME_PLAY){
			   setGameStatus(GAME_PAUSE);
		   }
		   soundPool.stop(alertStreamID);
		   allowToPlayAlert = false;
		   isPlayingAlert =false;
		   
		  
		   Music.stop(this);
				   
	   }
	   private int getGameState(){
		   return gameStatus;
	   }

	   
		private String randomizeWord(String solution) {
			List<String> ls = new ArrayList<String>();
			for(int i = 0; i < solution.length(); i++){
				ls.add(String.valueOf(solution.charAt(i)));
			}
			Collections.shuffle(ls);
			StringBuffer sb = new StringBuffer();
			for(Iterator<String> it = ls.iterator(); it.hasNext();){
				sb.append(it.next());
			}
			String result = new String(sb);
			return result;
			
		}

	String getWord(int length){
		Random r= new Random(System.currentTimeMillis());
		int fileIndex = r.nextInt(numRoot[length-3]);
		int wordIndex = r.nextInt(numRoot[length-3]);
		String filename =length+"_"+fileIndex;
		Log.d(TAG, fileIndex+"  "+wordIndex);
		int count = r.nextInt(wordIndex);
		String result="ERROR";
		try{
    		InputStream is = getAssets().open(filename);
    		InputStreamReader isReader = new InputStreamReader(is);
    		BufferedReader bReader = new BufferedReader(isReader);
    		result = null;
    		for(int i =0; i < count - 1; i++){
    			bReader.readLine();
    		}
    		result = bReader.readLine();
    		
    		
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
		return result;
	   }
	   
	   boolean inDictionary(String word){
	    	boolean result = false;
	    	String filename = word.length()+"/" +word.substring(0,2);
	    	
	    	try{
	    		InputStream is = getAssets().open(filename);
	    		InputStreamReader isReader = new InputStreamReader(is);
	    		BufferedReader bReader = new BufferedReader(isReader);
	    		String temp = null;
	    		while((temp = bReader.readLine())!=null){
	    			if(temp.equals(word)){
	    				return true;
	    			}
	    		}
	    		
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	return result;
	    }

	   
	   String getSolution(){
		   return solution;
	   }
	   
	   String getBoardWord(){
		   return boardWord;
	   }

		public void Quit() {
			finish();
			
			
		}

		/*public void run() {
			
			//long startTime = System.currentTimeMillis();
			while(!destroyTimer){
				try{
					Thread.sleep(1000);
					if(gameStatus == GAME_PLAY){
						timeLeft--;
					}
					if(timeLeft<=10){
						useAlertPaint = !useAlertPaint;
						if(!isPlayingAlert && allowToPlayAlert){
							alertStreamID=soundPool.play(DABBLE_ALERT, 1.0f, 1.0f, 0, -1, 1);
							isPlayingAlert = true;
						}
					}
					if(timeLeft == 0){
						setGameStatus(GAME_LOSE);
						if(!isPlayingLose){
							soundPool.play(DABBLE_LOSE, 1.0f, 1.0f, 0, 0, 1);
							isPlayingLose = true;
							Music.stop(this);
							soundPool.stop(alertStreamID);
							allowToPlayAlert = false;
							
						}
					}
					playerStatusText.setText(playerStatus);
					opponentStatusText.setText(opponentStatus);
				} catch (InterruptedException e){
					e.printStackTrace();
				}
			}
			
		}*/
	    public class AsyncTimer extends AsyncTask<Void, Integer,Void>{

	    	@Override
	    	protected Void doInBackground(Void... arg0) {

				while(!destroyTimer){
					try{
						publishProgress();
						Thread.sleep(100);
						if(gameStatus == GAME_PLAY){
							timeLeft-=100;
						}
						if(timeLeft<=GAME_TIMEALERT){
							publishProgress(GAME_ALEART);
							if(!isPlayingAlert && allowToPlayAlert){
								alertStreamID=soundPool.play(DABBLE_ALERT, 1.0f, 1.0f, 0, -1, 1);
								isPlayingAlert = true;
							}
						}
						if(timeLeft <= 0 && gameStatus!=GAME_LOSE){
							publishProgress(GAME_LOSE);
							if(!isPlayingLose){
								soundPool.play(DABBLE_LOSE, 1.0f, 1.0f, 0, 0, 1);
								isPlayingLose = true;
								Music.stop(SingleGame.this);
								soundPool.stop(alertStreamID);
								allowToPlayAlert = false;
								
							}
						}

					} catch (InterruptedException e){
						e.printStackTrace();
					}
					
				}
				return null;
	    	}
		    protected void onProgressUpdate(Integer... progress) {
		    	if(progress.length==1 && progress[0] ==GAME_ALEART){
		    		timeText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
		    	}
		    	if(progress.length==1 && progress[0] == GAME_LOSE){
		    		setGameStatus(GAME_LOSE);
		    	}
		    	super.onProgressUpdate(progress);
		    	playerStatusText.setText(playerStatus);
				opponentStatusText.setText(opponentStatus);
				scoreText.setText("Score:"+score);
				timeText.setText("Time:"+timeLeft/1000);
			   switch(gameStatus){
			   case GAME_PLAY:
				   pauseButton.setText("Pause");
				   break;
			   case GAME_PAUSE:
				   pauseButton.setText("Continue");
				   break;
			   case GAME_WIN:
				   pauseButton.setText("New Game");
				   break;
			   case GAME_LOSE:
				   pauseButton.setText("New Game");
				   break;
			   }
		     }

		    	protected void  onPostExecute(Void result){
		    		
		    		
		    		
					

		    }
	    }
	    	


		   public class Letter {
			   Boolean isSelected=false;
			   Boolean isValid = false;
		
				int index;
				RelativeLayout tile;
				TextView letter;
				TextView score;
				   Letter(){
				   }
			   
			   public void setInvisible(boolean b) {
					if(b){
						letter.setVisibility(View.INVISIBLE);
						score.setVisibility(View.INVISIBLE);
					} else{
						letter.setVisibility(View.VISIBLE);
						score.setVisibility(View.VISIBLE);
					}

				}

			public void setValid(boolean valid) {
				isValid = valid;
				if(valid){
					tile.setBackgroundResource(android.R.drawable.btn_star_big_on);
				} else {
					tile.setBackgroundResource(android.R.drawable.btn_default);
					
				}
				
			}

			public void setLetter(String l) {
				   	letter.setText(l);
				
			}

			public void setIndex(int i) {
				   index = i;
				
			}



			public String getLetter(){
				return (String) letter.getText();
				   
			   }

			   public void setSelected(){
				   isSelected = true;
				   tile.setBackgroundResource(android.R.drawable.button_onoff_indicator_on);
			   }
			   
			   public void setUnSelected(){
				   isSelected = false;
				   tile.setBackgroundResource(android.R.drawable.btn_default);
			   }
				Letter(RelativeLayout tiles, TextView l, TextView s, int i){
					tile = tiles;
					score = s;
					index = i;
					letter = l;
				   }



				public void setScore(String s) {
					score.setText(s);
					
				}
				
				public String getScore(){
					return score.getText().toString();
				}

				public void setLetter(char l) {
					letter.setText(Character.toString(l));
					
				}
			   
		   }
		   private void iniLetters(){
				 
			   for(int i = 0; i < 18; i++){
				   Letter temp = new Letter(tiles[i],tilesLetter[i],tilesScore[i],i);
				   temp.setLetter(boardWord.charAt(i));
				   
				   temp.setScore(getScoreForLetter(boardWord.charAt(i)));
				   Log.d(TAG,"Add letter");
				   letters.add(temp);
			   }
			   
			   checkValidWords();
		   }
		   
			private void restartGame() {
				iniGame();
				letters.clear();
				iniLetters();

				setGameStatus(GAME_PLAY);
				setShowHint(false);
				timeLeft = GAME_TIME;
				score = 0;
				timeText.setTextColor(getResources().getColor(android.R.color.black));
				Log.d(TAG, "Set time color");
				isPlayingLose = false;
				isPlayingAlert = false;
				useAlertPaint  = false;
				allowToPlayAlert = true;
				selectedTileIndex = NOTILEISSELECTED;
				playerStatus = "";
				opponentStatus = "";
				pauseButton.setText("Pause");
				Music.play(this, R.raw.dabble_background);
				new AsyncTimer().execute();
				// Reset Score, timer....
			}
			   private void setGameStatus(int status){
				   switch(status){
				   case GAME_PLAY:
					   addPlayerMessage(" Game Started!"+"\n");

					   break;
				   case GAME_PAUSE:
					   addPlayerMessage(" Game Paused!"+"\n");

					   break;
				   case GAME_WIN:
					   addPlayerMessage(" You Win!"+"\n");

					   break;
				   case GAME_LOSE:
					   addPlayerMessage(" Time up!"+"\n");

					   break;
				   }
				   gameStatus = status;
			   }
			   private void setShowHint(boolean show){
				   if(show){
					   String hint = solution.substring(0,3)+"\n"+solution.substring(3,7)+"\n"+solution.substring(7,12)+"\n"+solution.substring(12,18)+"\n";
					   opponentStatus = hint;
					   
				   } else {
					   opponentStatus = "";
					   
				   }
				   showHint = show;
			   }
			   public static String getTimeShort() {
				   SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
				   Date currentTime = new Date();
				   String dateString = formatter.format(currentTime);
				   return dateString;
				  }
			   
			   private void addPlayerMessage(String message){
				   playerStatus+=getTimeShort()+"\n"+message;
				   playerStatusText.setText(playerStatus);
				   playerStatusScroll.post(new Runnable(){
					   public void run(){
						   playerStatusScroll.fullScroll(View.FOCUS_DOWN);
					   }
				   });
				   //playerStatusScroll.fullScroll(View.FOCUS_DOWN);
			    	
			   }
			   
}
