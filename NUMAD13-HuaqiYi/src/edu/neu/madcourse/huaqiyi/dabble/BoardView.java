package edu.neu.madcourse.huaqiyi.dabble;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import edu.neu.madcourse.huaqiyi.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;

public class BoardView extends View implements Runnable{


	  
	   private static final String TAG = "Dabble";

	   //private static final int ID = 42; 
	   
	   private float width;
	   private float height;
	   private float menuWidth;
	   private float menuHeight;
	   private float menuFont_size;
	   private float hintStartX;
	   private float hintStartY;
	   private float phoneW;
	   private float phoneH;
	   private final Game game;
	   
	   private ArrayList<Letter> letters = new ArrayList<Letter>();
	   private boolean isDragging = false;
	   private boolean destroyTimer = false;
	   float font_size;
	   Paint selectedPaint;
	   Paint unSelectedPaint;
	   Paint validPaint;
	   Paint menuPaint;
	   Paint hintPaint;
	   Paint timerPaint;
	   Paint timerAlertPaint;
	   Paint background;
	   private  int gameState;
	   public final int GAME_PLAY = 1;
	   public final int GAME_PAUSE = 2;
	   public final int GAME_WIN = 3;
	   public final int GAME_LOSE =4;
	   private final int GAME_TIME = 60;
	   private final int GAME_TIMEALERT = 30;
	   private boolean useAlertPaint = false;
	   public boolean isPlayingAlert = false;
	   private int timeLeft;
	   private int score;
	   Path leftButton = new Path();
	   Path middleButton = new Path();
	   Path rightButton = new Path();
	   boolean isPlayingLose = false;
	   public boolean allowToPlayAlert = false;
	   int alertStreamID=0;
	   final int LEFT = 0;
	   final int MIDDLE = 1;
	   final int RIGHT =2;
	   boolean showHint = false;
	   FontMetrics mfm;
	   FontMetrics fm;
		public BoardView(Context context) {
			super(context);
			timeLeft = GAME_TIME;
			score=0;
			gameState = GAME_PLAY;
			allowToPlayAlert = true;
			Log.d(TAG,"Constructor BoardView");
			this.game = (Game) context;
			setFocusable(true);
			setFocusableInTouchMode(true);
			Log.d(TAG,"Set Focusalbe");
			//letters = game.getLetters();
			selectedPaint = new Paint();
			selectedPaint.setColor(Color.RED);
			selectedPaint.setStyle(Style.STROKE);
			
			unSelectedPaint = new Paint();
			unSelectedPaint.setColor(Color.BLACK);
			unSelectedPaint.setStyle(Style.STROKE);
			
			validPaint = new Paint();
			validPaint.setColor(Color.BLUE);
			validPaint.setStyle(Style.STROKE);
			
			menuPaint = new Paint();
			menuPaint.setColor(Color.BLACK);
			menuPaint.setStyle(Style.STROKE);
			
			hintPaint = new Paint();
			hintPaint.setColor(Color.BLACK);
			hintPaint.setStyle(Style.STROKE);		
			
			timerPaint = new Paint();
			timerPaint.setColor(Color.BLACK);
			timerPaint.setStyle(Style.STROKE);	
			
			timerAlertPaint = new Paint();
			timerAlertPaint.setColor(Color.RED);
			timerAlertPaint.setStyle(Style.STROKE);	
			
			background = new Paint();
			Thread timer = new Thread(this);
			timer.start();
			
			//setId(ID);

		}
	   
	   private RectF makeMenuButton(int i) {
			RectF result = new RectF();
			int left = (int)(menuWidth/6 + i* menuWidth * 4/3);
			int top = (int)(4*height + height/4);
			int right = (int)(left+menuWidth);
			int bottom = (int) (top + menuHeight);
			result.set(left, top, right,bottom);
			Log.d(TAG,"MAKING BUTTON "+left+"  "+top+"  "+right+"  "+bottom);
			return result;
		}

	@Override
	   protected Parcelable onSaveInstanceState() { 
	      Parcelable p = super.onSaveInstanceState();
	      Log.d(TAG, "onSaveInstanceState");
	      Bundle bundle = new Bundle();
	      //bundle.putInt(SELX, selX);
	      //bundle.putInt(SELY, selY);
	      //bundle.putParcelable(VIEW_STATE, p);
	      return bundle;
	   }
	   @Override
	   protected void onRestoreInstanceState(Parcelable state) { 
	      Log.d(TAG, "onRestoreInstanceState");
	      //Bundle bundle = (Bundle) state;
	      //select(bundle.getInt(SELX), bundle.getInt(SELY));
	      //super.onRestoreInstanceState(bundle.getParcelable(VIEW_STATE));
	      super.onRestoreInstanceState(state);
	   }
	   

	   @Override
	   protected void onSizeChanged(int w, int h, int oldw, int oldh) {
	      phoneW = w;
	      phoneH = h;
	      height = h / 5f;
	      width = height;
	      menuHeight = 0.75f * height;
	      menuWidth = 0.25f * w;
	      font_size = height * 0.75f;
	      menuFont_size = menuHeight * 0.75f;
	      hintStartX = w;
	      hintStartY = 2.5f*height; 
	      //getRect(selX, selY, selRect);
	      Log.d(TAG, "onSizeChanged: width " + width + ", height "
	            + height);
	      unSelectedPaint.setTextSize(font_size);
	      unSelectedPaint.setTextAlign(Align.CENTER);
	      selectedPaint.setTextSize(font_size);
	      selectedPaint.setTextAlign(Align.CENTER);
	      validPaint.setTextSize(font_size);
	      validPaint.setTextAlign(Align.CENTER);
	      menuPaint.setTextSize(menuFont_size);
	      menuPaint.setTextAlign(Align.CENTER);
	      hintPaint.setTextSize(0.5f*height);
	      hintPaint.setTextAlign(Align.RIGHT);
	      timerPaint.setTextSize(0.5f*height);
	      timerPaint.setTextAlign(Align.RIGHT);
	      timerAlertPaint.setTextSize(0.5f*height);
	      timerAlertPaint.setTextAlign(Align.RIGHT);
	      fm = selectedPaint.getFontMetrics();
	      mfm = menuPaint.getFontMetrics();
	      iniLetters();
	      leftButton.addRect(makeMenuButton(LEFT), Direction.CW);
	      middleButton.addRect(makeMenuButton(MIDDLE), Direction.CW);
	      rightButton.addRect(makeMenuButton(RIGHT), Direction.CW);
	      super.onSizeChanged(w, h, oldw, oldh);
	   }

	   @Override
	   protected void onDraw(Canvas canvas) {
		   //Log.d(TAG, "BoardView onDraw");
	      // Draw the background...
	      
	      background.setColor(getResources().getColor(
	            R.color.puzzle_background));
	      canvas.drawRect(0, 0, getWidth(), getHeight(), background);

	      
	      // Draw the board...
	      
	      // Define colors for the grid lines
	      switch(gameState){
	      	case GAME_PLAY: 
	      		for(Iterator<Letter> it = letters.iterator(); it.hasNext();){
	      			it.next().draw(canvas);
	      		}
	      		break;
	      	case GAME_WIN:
	      		for(Iterator<Letter> it = letters.iterator(); it.hasNext();){
	      			it.next().draw(canvas);
	      		}
	      		drawWin(canvas);
	      		break;
	      	case GAME_LOSE:
	      		for(Iterator<Letter> it = letters.iterator(); it.hasNext();){
	      			it.next().draw(canvas);
	      		}
	      		drawLose(canvas);
	      		break;
	      }
	      drawTime(canvas);
	      drawMenu(canvas);
	      drawScore(canvas);
	      if(showHint) {
	    	  drawHint(canvas);
	      }
	   }
	    	  


	   private void drawScore(Canvas canvas) {
		   canvas.drawText("Score: "+score, hintStartX, height, timerPaint);
		
	}

	private void drawTime(Canvas canvas) {
		   if(!useAlertPaint){
			   canvas.drawText("Time Remaining: "+timeLeft, hintStartX, height/2, timerPaint);
		   } else {
			   canvas.drawText("Time Remaining: "+timeLeft, hintStartX, height/2, timerAlertPaint);
		   }
	}

	private void drawLose(Canvas canvas) {
		   canvas.drawText("Time Up", phoneW, phoneH/2-height/2, timerPaint);
		
	}

	private void drawWin(Canvas canvas) {
		   canvas.drawText("Congratulation! You Spelt All Words!", phoneW, phoneH/2-height/2, timerAlertPaint);
		
	}

	private void drawHint(Canvas canvas) {
		canvas.drawText(game.solution.substring(0,3), hintStartX, hintStartY, hintPaint);
		canvas.drawText(game.solution.substring(3,7), hintStartX, hintStartY+height/2, hintPaint);
		canvas.drawText(game.solution.substring(7,12), hintStartX, hintStartY+2*height/2, hintPaint);
		canvas.drawText(game.solution.substring(12,18), hintStartX, hintStartY+3*height/2, hintPaint);
		
	}

	private void drawMenu(Canvas canvas) {
		   float offsety = (mfm.ascent+mfm.descent)/2;


			
			for (int i = 0; i < 3; i++ ){
				int left = (int)(menuWidth/6 + i* menuWidth * 4/3);
				int top = (int)(4*height + height/4);
				int right = (int)(left+menuWidth);
				int bottom = (int) (top + menuHeight);
				switch(i){
				case LEFT:
					canvas.drawPath(leftButton, menuPaint);
					switch(gameState){
					case GAME_PLAY: 
						canvas.drawText("PAUSE",(left+right)/2f ,(top+bottom)/2f - offsety, menuPaint);
						break;
					case GAME_PAUSE:
						canvas.drawText("RESUME",(left+right)/2f ,(top+bottom)/2f - offsety, menuPaint);
						break;
					case GAME_LOSE:
					case GAME_WIN:
						canvas.drawText("NEW",(left+right)/2f ,(top+bottom)/2f - offsety, menuPaint);
						break;
					}
					break;
				case MIDDLE:
					canvas.drawPath(middleButton, menuPaint);
					canvas.drawText("HINT",(left+right)/2f ,(top+bottom)/2f - offsety, menuPaint);
					break;
				case RIGHT:
					canvas.drawPath(rightButton, menuPaint);
					canvas.drawText("QUIT",(left+right)/2f ,(top+bottom)/2f - offsety, menuPaint);
					break;
				}

			}
		
	}



	@Override
	   public boolean onTouchEvent(MotionEvent event) {

		onTouchLettersEvent(event);
		onTouchMenuEvent(event);
	    return true;
	   }

		
	   private void onTouchMenuEvent(MotionEvent event) {
		
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			for(int i = LEFT; i <= RIGHT; i++){
				if(selectedMenu(i, event.getX(),event.getY())){
					Log.d(TAG, "TOUCHED BUTTON "+i);
					switch(i){
					case LEFT:
						switch(gameState){
						case GAME_PLAY:
							gameState = GAME_PAUSE;
							allowToPlayAlert = false;
							break;
						case GAME_PAUSE:
							gameState = GAME_PLAY;
							allowToPlayAlert = true;
							break;
						case GAME_WIN:
						case GAME_LOSE:
							restartGame();
							break;
						}
						break;
					case MIDDLE:
						showHint = !showHint;
						break;
					case RIGHT:
						destroyTimer=true;
						game.soundPool.stop(alertStreamID);
						isPlayingAlert = false;
						allowToPlayAlert =false;
						game.Quit();
						break;
					}
					game.soundPool.play(game.DABBLE_SELECT, 1.0f, 1.0f, 0, 0, 1);
				}
			}
		}
		invalidate();
		
	}

	private void restartGame() {
		game.iniGame();
		letters.clear();
		iniLetters();
		gameState = GAME_PLAY;
		showHint = false;
		timeLeft = GAME_TIME;
		score = 0;
		isPlayingLose = false;
		isPlayingAlert = false;
		useAlertPaint = false;
		allowToPlayAlert = true;
		Music.play(game, R.raw.dabble_background);
		// Reset Score, timer....
	}

	private boolean selectedMenu(int i, float mx, float my) {
		int left = (int)(menuWidth/6 + i* menuWidth * 4/3);
		int top = (int)(4*height + height/4);
		int right = (int)(left+menuWidth);
		int bottom = (int) (top + menuHeight);
		return (mx <= right && my <= bottom && mx >= left && my >= top);

	}

	private void onTouchLettersEvent(MotionEvent event) {
		if(gameState == GAME_PLAY){
			
			switch(event.getAction()){
				case MotionEvent.ACTION_DOWN:
					if(!isDragging){
						for(Iterator<Letter> it = letters.iterator(); it.hasNext();){
							Letter l = it.next();
							if(l.ifSelected(event.getX(),event.getY())){
								l.setSelected();
								Log.d(TAG, l.getLetter()+" is Selected");
								game.soundPool.play(game.DABBLE_SELECT, 1.0f, 1.0f, 0, 0, 1);
							}
						}
						isDragging = true;
					}
					break;
				case MotionEvent.ACTION_MOVE:
					if(isDragging){
						for(Iterator<Letter> it = letters.iterator(); it.hasNext();){
							Letter l = it.next();
							if(l.isSelected){
								l.moveTo(event.getX(),event.getY());
								Log.d(TAG, l.getLetter()+" is Dragged");
							}
						}
					}
					break;
				case MotionEvent.ACTION_UP:
					Log.d(TAG, "Touch Up");
					
					boolean selectedAnother = false;
					Letter selectedL = null;
					Letter anotherL = null;
					
					for(Iterator<Letter> it = letters.iterator(); it.hasNext();){
						Letter l = it.next();
						if(l.isSelected) {
							selectedL = l;
							game.soundPool.play(game.DABBLE_SELECT, 1.0f, 1.0f, 0, 0, 1);
						}
						if(!l.isSelected && l.ifSelected(event.getX(),event.getY()) && !selectedAnother){
							Log.d(TAG, "Selected Another Letter "+l.getLetter());
							anotherL=l;
							anotherL.setSelected();
							selectedAnother = true;
						} 
							
					}
					if(selectedAnother) {
						Log.d(TAG, "SWAP "+selectedL.getLetter()+" with "+anotherL.getLetter());
						swapLetter(selectedL,anotherL);
						checkValidWords();
						
					} else if(selectedL!=null){
						Log.d(TAG,"Put "+selectedL.getLetter()+" Back to index "+ selectedL.index);
						selectedL.moveTo(selectedL.index);
						
					}
					isDragging = false;
					
					for(Iterator<Letter> it = letters.iterator(); it.hasNext();){
						Letter l = it.next();
						if(l.isSelected) {
							Log.d(TAG, "Unselected "+l.getLetter());
							l.setUnSelected();
						}
					}
					break;
				}
				invalidate();
			}
		
	}

	public int getGameState(){
		return gameState;
	}
	
	public void setGameState(int state){
		gameState = state;
	}
	private void checkWin(boolean[] isValid) {
		if(isValid[0] && isValid[1] && isValid[2] && isValid[3] ){
			gameState=GAME_WIN;
			score+= timeLeft;
			Music.stop(game);
			game.soundPool.play(game.DABBLE_WIN, 1.0f, 1.0f, 0, 0, 1);
			game.soundPool.stop(alertStreamID);
		}
		
	}

	private void checkValidWords() {
		String firstRow="";
		String secondRow="";
		String thirdRow="";
		String fourthRow="";
		for(int i = 0 ; i < 3 ; i++){
			firstRow += letters.get(i).getLetter();
			
		}
		Log.d(TAG, "First word is"+firstRow);
		
		for(int i = 3 ; i < 7 ; i++){
			secondRow += letters.get(i).getLetter();
			
		}
		Log.d(TAG, "First word is"+secondRow);
		
		for(int i = 7 ; i < 12 ; i++){
			thirdRow += letters.get(i).getLetter();
			
		}
		Log.d(TAG, "First word is"+thirdRow);
		
		for(int i = 12 ; i < 18 ; i++){
			fourthRow += letters.get(i).getLetter();
			
		}
		Log.d(TAG, "First word is"+fourthRow);
		boolean[] isValid = new boolean[]{false,false,false,false};
		isValid[0] = game.inDictionary(firstRow.toLowerCase());
		isValid[1] = game.inDictionary(secondRow.toLowerCase());
		isValid[2] = game.inDictionary(thirdRow.toLowerCase());
		isValid[3] = game.inDictionary(fourthRow.toLowerCase());
		if(isValid[0] || isValid[1] || isValid[2] || isValid[3]){
			game.soundPool.play(game.DABBLE_VALID, 1.0f, 1.0f, 0, 0, 1);
		}
		for(int i = 0 ; i < 3 ; i++){
			
			letters.get(i).setValid(isValid[0]);

		}
	
		
		

		for(int i = 3 ; i < 7 ; i++){
			letters.get(i).setValid(isValid[1]);
			//Log.d(TAG, secondRow+" First word is Valid");

		}
		
		
		

		for(int i = 7 ; i < 12 ; i++){
			letters.get(i).setValid(isValid[2]);
			//Log.d(TAG, thirdRow+" Third word is valid");

		}
		
		
		

		for(int i = 12 ; i < 18 ; i++){
			letters.get(i).setValid(isValid[3]);
			//Log.d(TAG, fourthRow + " Fourth word is valid");

		}
		score = 0;
		for(int i = 0; i < 4; i++){
			if(isValid[i]){
				score += getScoreInRow(i);
			}
		}
		checkWin(isValid);
	}

	private int getScoreInRow(int i) {
		int result = 0;
		switch(i){
		case 0:
			
			for(int j = 0; j < 3; j ++){
				result+=letters.get(j).getScore();
			}
			return result * 3;
		case 1:

			for(int j = 3; j < 7; j ++){
				result+=letters.get(j).getScore();
			}
			return result * 4;
			
		case 2:

			for(int j = 7; j < 12; j ++){
				result+=letters.get(j).getScore();
			}
			return result * 5;
			
		case 3:

			for(int j = 12; j < 18; j ++){
				result+=letters.get(j).getScore();
			}
			return result * 6;
			
		}
		return 0;
	}

	private void swapLetter(Letter firstL, Letter secondL) {
		//firstL.setUnSelected();
		//secondL.setUnSelected();
		   int firstIndex = firstL.index;
		   int secondIndex = secondL.index;
		   int firstScore = firstL.score;
		   int secondScore = secondL.score;
		   firstL.moveTo(firstIndex);
		   secondL.moveTo(secondIndex);
		   char firstLetter = firstL.getLetter();
		   char secondLetter = secondL.getLetter();
		   
		   firstL.setLetter(secondLetter);
		   firstL.setScore(secondScore);
		   
		   secondL.setLetter(firstLetter);
		   secondL.setScore(firstScore);
		// TODO Auto-generated method stub
		   // Also change the score....
		   Letter temp = null;
		   
		
	}

	@Override
	   public boolean onKeyDown(int keyCode, KeyEvent event) {

	      return super.onKeyDown(keyCode, event);
	   }


	   
	   public Letter getLetter(int i, char l){
		   return new Letter(i,l);
	   }

	   public class Letter {
		   char letter;
		   Boolean isSelected=false;
		   Boolean isValid = false;
		   int left;
		   int top;
		   int right;
		   int bottom;
		   int score;
		   int index;
		   Path rect = new Path();
		   Letter(){
		   }
		   
		   public void setValid(boolean valid) {
			isValid = valid;
			
		}

		public void setLetter(char l) {
			   	letter = l;
			
		}

		public void setIndex(int i) {
			   index = i;
			
		}

		public void moveTo(int i) {
			   rect.rewind();
			   rect.addRect(makeRect(i), Direction.CW);
			
		}

		public char getLetter(){
			return letter;
			   
		   }
		   public boolean ifSelected(float mx, float my) {
			   
			   return (mx <= right && my <= bottom && mx >= left && my >= top);
			
		}
		   public void setSelected(){
			   isSelected = true;
		   }
		   
		   public void setUnSelected(){
			   isSelected = false;
		   }

		public void moveTo(float x, float y) {
			rect.rewind();
			rect.addRect(makeRect(x,y), Direction.CW);
			
		}

		private RectF makeRect(float centerX, float centerY) {
			RectF result = new RectF();
			left = (int) (centerX-width/2);
			top = (int) (centerY-width/2);
			right = (int) (centerX+width/2);
			bottom = (int) (centerY+width/2);
			result.set(left, top, right,bottom);
			
			return result;
		}

		public void draw(Canvas canvas) {
			   float offsety = (fm.ascent+fm.descent)/2;

			   if(isSelected){
				  selectedPaint.setTextSize(font_size);
				  selectedPaint.setTextAlign(Align.CENTER);
				  canvas.drawPath(rect, selectedPaint);
				  canvas.drawText(String.valueOf(letter),(left+right)/2f ,(top+bottom)/2f - offsety, selectedPaint);
				  selectedPaint.setTextSize(font_size/2);
				  selectedPaint.setTextAlign(Align.RIGHT);
				  canvas.drawText(score+"",(left+right)/2f+height/2,(top+bottom)/2f - offsety + width/4, selectedPaint);
			   } else if(isValid){
					  validPaint.setTextSize(font_size);
					  validPaint.setTextAlign(Align.CENTER);
					  canvas.drawPath(rect, validPaint);
					  canvas.drawText(String.valueOf(letter),(left+right)/2f ,(top+bottom)/2f - offsety, validPaint);
					  validPaint.setTextSize(font_size/2);
					  validPaint.setTextAlign(Align.RIGHT);
					  canvas.drawText(score+"",(left+right)/2f+height/2,(top+bottom)/2f - offsety + width/4, validPaint);
			   } else{
				  unSelectedPaint.setTextSize(font_size);
				  unSelectedPaint.setTextAlign(Align.CENTER);
				  canvas.drawPath(rect, unSelectedPaint);
				  canvas.drawText(String.valueOf(letter),(left+right)/2f ,(top+bottom)/2f - offsety, unSelectedPaint);
				  unSelectedPaint.setTextSize(font_size/2);
				  unSelectedPaint.setTextAlign(Align.RIGHT);
				  canvas.drawText(score+"",(left+right)/2f+height/2,(top+bottom)/2f - offsety + width/4, unSelectedPaint);
			   }
			   
			   invalidate();
		   }

			Letter(int i, char l){
				   index = i;
				   letter = l;
				   rect.addRect(makeRect(index), Direction.CW);
				   
			   }
			RectF makeRect(int i){
				index = i;
				int x = getXFromIndex(i);
				int y = getYFromIndex(i);
				RectF result = new RectF();
				left = (int)(x*width);
				top = (int)(y*width);
				right = (int)((x+1)*width);
				bottom = (int) ((y+1)*width);
				result.set(left, top, right,bottom);
				Log.d(TAG,(int)(x*width)+" "+(int)(y*width)+" "+(int)((x+1)*width)+" "+(int) ((y+1)*width));
				return result;
				
			}
	
			private int getYFromIndex(int i) {
				if(0<=index && index<=2){
					return 0;
				} else if (3<=index && index<=6){
					return 1;
				} else if (7<=index && index<=11) {
					return 2;
					
				} else if (12<=index && index<=17){
					return 3;
				}
				return -1;
			}
	
			private int getXFromIndex(int i) {
				if(0<=index && index<=2){
					return index;
				} else if (3<=index && index<=6){
					return index-3;
				} else if (7<=index && index<=11) {
					return index-7;
					
				} else if (12<=index && index<=17){
					return index-12;
				}
				return -1;
			}

			public void setScore(int i) {
				score = i;
				
			}
			
			public int getScore(){
				return score;
			}
		   
	   }
	   private void iniLetters(){
			 
		   for(int i = 0; i < 18; i++){
			   Letter temp = new Letter(i, game.boardWord.charAt(i));
			   temp.setScore(3+(i*i*i*i*9/(17*17*16*16)));
			   Log.d(TAG,"Add letter");
			   letters.add(temp);
		   }
		   
		   checkValidWords();
	   }

	@Override
	public void run() {
		
		//long startTime = System.currentTimeMillis();
		while(!destroyTimer){
			try{
				Thread.sleep(1000);
				if(gameState == GAME_PLAY){
					timeLeft--;
				}
				if(timeLeft<=10){
					useAlertPaint = !useAlertPaint;
					if(!isPlayingAlert && allowToPlayAlert){
						alertStreamID=game.soundPool.play(game.DABBLE_ALERT, 1.0f, 1.0f, 0, -1, 1);
						isPlayingAlert = true;
					}
				}
				if(timeLeft == 0){
					gameState=GAME_LOSE;
					if(!isPlayingLose){
						game.soundPool.play(game.DABBLE_LOSE, 1.0f, 1.0f, 0, 0, 1);
						isPlayingLose = true;
						Music.stop(game);
						game.soundPool.stop(alertStreamID);
						allowToPlayAlert = false;
						
					}
				}
			} catch (InterruptedException e){
				e.printStackTrace();
			}
		}
		
	}
}
