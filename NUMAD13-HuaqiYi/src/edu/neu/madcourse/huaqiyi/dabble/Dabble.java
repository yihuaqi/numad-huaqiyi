/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
***/
package edu.neu.madcourse.huaqiyi.dabble;

import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.R.array;
import edu.neu.madcourse.huaqiyi.R.id;
import edu.neu.madcourse.huaqiyi.R.layout;
import edu.neu.madcourse.huaqiyi.R.menu;
import edu.neu.madcourse.huaqiyi.R.string;
import edu.neu.madcourse.huaqiyi.dabbletest.SingleGame;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

public class Dabble extends Activity implements OnClickListener {
   private static final String TAG = "Dabble";
   
   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Log.d(TAG,"Dabble onCreate");
      requestWindowFeature(Window.FEATURE_NO_TITLE);
      getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
      WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
      setContentView(R.layout.dabble_main);

      // Set up click listeners for all the buttons
      View newButton = findViewById(R.id.dabble_new_button);
      newButton.setOnClickListener(this);
      View rulesButton = findViewById(R.id.dabble_rules_button);
      rulesButton.setOnClickListener(this);
      View acknowledgesButton = findViewById(R.id.dabble_acknowledges_button);
      acknowledgesButton.setOnClickListener(this);
      View quitButton = findViewById(R.id.dabble_quit_button);
      quitButton.setOnClickListener(this);
   }

   @Override
   protected void onResume() {
      super.onResume();
      //TODO get music;
      Music.play(this, R.raw.dabble_background);
      
   }

   @Override
   protected void onPause() {
      super.onPause();
      Music.stop(this);
   }

   public void onClick(View v) {
	   Intent i;
      int id = v.getId();
	if (id == R.id.dabble_new_button) {
		i = new Intent(this, SingleGame.class);
		startActivity(i);
	} else if (id == R.id.dabble_rules_button) {
		i = new Intent(this, Rules.class);
		startActivity(i);
	} else if (id == R.id.dabble_acknowledges_button) {
		i = new Intent(this, Acknowledgements.class);
		startActivity(i);
	} else if (id == R.id.dabble_quit_button) {
		finish();
	}
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      super.onCreateOptionsMenu(menu);
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.dabble_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int itemId = item.getItemId();
	if (itemId == R.id.dabble_settings) {
		startActivity(new Intent(this, Prefs.class));
		return true;
      // More items go here (if any) ...
	}
      return false;
   }

  


   /** Start a new game with the given difficulty level */
   private void startGame(int i) {
      Log.d(TAG, "clicked on " + i);
      Intent intent = new Intent(this, Game.class);
      //intent.putExtra(Game.KEY_DIFFICULTY, i);
      startActivity(intent);
   }
}