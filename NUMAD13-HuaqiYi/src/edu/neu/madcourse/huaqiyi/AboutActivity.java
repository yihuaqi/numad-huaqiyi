package edu.neu.madcourse.huaqiyi;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.TextureView;
import android.widget.TextView;

public class AboutActivity extends Activity{
	
	TextView uniqueIdText;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        uniqueIdText = (TextView) findViewById(R.id.uniqueIdText);
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        
        uniqueIdText.setText("DeviceId:"+telephonyManager.getDeviceId());
	}

}
