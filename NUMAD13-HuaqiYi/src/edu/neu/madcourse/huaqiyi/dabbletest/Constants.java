package edu.neu.madcourse.huaqiyi.dabbletest;

public  class Constants {
	//public static final String OPPONENTNAME = "OpponentName";
	//public static final String GAMEID = "GameId";
	static String TEAMNAME = "danganronpa";
	static String PASSWORD = "monokuma";
	static String HIGHSCORE = "highscore";
	private static final String RECORDSEPARATOR = "___";
	public static final String HAS_BOARD = "has_board";
	public static final String EMPTYRECORD = "";
	static String NAMESCORESEPARATOR = ":";
	static String PLAYERNAMEKEY = "PlayerName";
	static String OPPONENTNAMEKEY = "OpponentName";
	static String GAMEIDKEY = "GameId";
	static String PLAYERSCOREKEY = "PlayerScore";
	static String PLAYERLIST = "PlayerList";
	static String ATTRIBUTEINDICATOR = ":";
	static String ATTRIBUTESEPERATOR = ";";
	static String GAMELIST = "GameList";
	static String GAMEINDICATOR = "GAME";
	static String EXCEPTIONNOSUCHKEY = "Error: No Such Key";
	static String USERNAME = "";
	static String OPPONENT = "";
	static String GAMEID = "";
	static String ATTRIBUTE_STATUS = "status";
	static String ATTRIBUTE_BOARD = "board";
	static String ATTRIBUTE_PLAYERSTATUS = "playerStatus";
	static String ATTRIBUTE_WORD_LIST = "wordlist";
	static String ATTRIBUTE_SCORE = "score";
	static String ATTRIBUTE_TURN = "turn";
	static String ATTRIBUTE_WIN = "win";
	static String OPPONENTSCORE = "";
	static String ATTRIBUTE_NOTIFICATION = "notification";
	

	public static String queryPlayerGameList(String playerName) {
		return playerName+ATTRIBUTEINDICATOR+GAMELIST;
	}
	public static String makeGameName(String timeStamp,String Host,String Chanllenger) {
		
		return Host+ATTRIBUTESEPERATOR+Chanllenger+ATTRIBUTESEPERATOR+timeStamp;
	}
	public static void setPlayer(String p){
		USERNAME = p;
	}
	public static String getPlayer(){
		return USERNAME;
	}
	public static void setOpponent(String o) {
		OPPONENT = o;
		
	}
	public static String getOpponent(){
		return OPPONENT;
	}
	public static void setGameId(String g) {
		GAMEID=g;
		
	}
	public static String getGameId(){
		return GAMEID;
		  
	}
	public static String appendPlayer(String players, String playerName) {
		
		return players+Constants.RECORDSEPARATOR+playerName;
	}
	public static String[] getPlayerList(String players) {
		return players.split(Constants.RECORDSEPARATOR);
	}
	public static String appendGame(String playerGames, String gameName) {
		return playerGames+Constants.RECORDSEPARATOR+gameName;
		
	}
	public static String[] getGameList(String games) {
		return games.split(Constants.RECORDSEPARATOR);
	}
	public static String[] getHighScoreList(String result) {

		return result.split(RECORDSEPARATOR);
	}
	public static String[] getHighScoreRecord(String HighScore) {
		
		return HighScore.split(Constants.NAMESCORESEPARATOR);
	}

	public static String makeHighScoreRecord(String name, String score) {
		
		return name+NAMESCORESEPARATOR+score;
	}
	public static String appendHighScores(String result,
			String record) {
		
		return result+RECORDSEPARATOR+record;
	}
	public static String[] getPlayerOpponent(String record){
		return record.split(" vs ");
	}
	
	public static String queryGameStatus(){
		return GAMEID+ATTRIBUTEINDICATOR+ATTRIBUTE_STATUS;
	}
	public static String queryGameBoard(){
		return GAMEID+ATTRIBUTEINDICATOR+ATTRIBUTE_BOARD;
	}
	public static String queryGamePlayerStatus(String player){
		return GAMEID+ATTRIBUTESEPERATOR+player+ATTRIBUTEINDICATOR+ATTRIBUTE_STATUS;
	}
	public static String queryGameWordList(String player){
		return GAMEID+ATTRIBUTESEPERATOR+player+ATTRIBUTEINDICATOR+ATTRIBUTE_WORD_LIST;
	}
	
	public static String queryGamePlayerScore(String player){
		return GAMEID+ATTRIBUTESEPERATOR+player+ATTRIBUTEINDICATOR+ATTRIBUTE_SCORE;
	}
	public static String makePlayerTurn(String player) {

		return player+ATTRIBUTEINDICATOR+ATTRIBUTE_TURN;
	}
	public static String getOpponentScore() {
		
		return OPPONENTSCORE;
	}
public static void setOpponentScore(String os) {
		
		OPPONENTSCORE=os;
	}
public static String makeGamePlayerWin(String player) {
	
	return player+ATTRIBUTEINDICATOR+ATTRIBUTE_WIN;
}
public static String[] getGameStatus(String gs) {
	String[] result = gs.split(ATTRIBUTEINDICATOR);
	return result;
}
public static String makeWords(String[] wordlist) {
	
	return wordlist[0]+ATTRIBUTESEPERATOR+wordlist[1]+ATTRIBUTESEPERATOR+wordlist[2]+ATTRIBUTESEPERATOR+wordlist[3];
}
public static String[] getWordList(String words) {
	// TODO Auto-generated method stub
	return words.split(ATTRIBUTESEPERATOR);
}
public static String queryNotificationId(String receiver) {
	return receiver + ATTRIBUTEINDICATOR + ATTRIBUTE_NOTIFICATION;
	
}
public static String makeNotificationId(String sender) {
	return sender+ATTRIBUTESEPERATOR+System.currentTimeMillis();
	
}
	
public static String[] getNotification(String nId, String message){
	String[] result = new String[3];
	String sender = nId.split(ATTRIBUTESEPERATOR)[0];
	String time = nId.split(ATTRIBUTESEPERATOR)[1];
	result[0] = sender;
	result[1] = time;
	result[2] = message;
	return result;
}
public static String appendNotification(String notifications,
		String NotificationId) {
	return notifications + RECORDSEPARATOR + NotificationId;
	
}
public static String[] getNotificationList(String notifications) {
	return notifications.split(RECORDSEPARATOR);
	
}
}