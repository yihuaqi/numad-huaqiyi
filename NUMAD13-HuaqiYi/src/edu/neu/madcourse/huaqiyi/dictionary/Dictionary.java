package edu.neu.madcourse.huaqiyi.dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.R.integer;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import edu.neu.madcourse.huaqiyi.MainActivity;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary_Service.DictionaryBinder;

public class Dictionary extends Activity{
	
	TextView displayedText;
	TextView statusText;
	EditText editText;
	Button clearButton;
	Button returnButton;
	Button acknowlegementsButton;
	String displayedWords="";
	ProgressBar dictionaryProgressBar;
	
	Dictionary_Service dService;
	boolean dBound = false;
	ToneGenerator tg;
	


	
    protected void onCreate(Bundle savedInstanceState) {
		
	tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.dictionary);

    
    
    displayedText = (TextView) findViewById(R.id.dictionaryDisplayedText);
    displayedText.setMovementMethod(ScrollingMovementMethod.getInstance());
    editText = (EditText) findViewById(R.id.dictionaryEditText);
    clearButton = (Button)findViewById(R.id.dictionaryClearButton);
    returnButton = (Button) findViewById(R.id.dictionaryReturnButton);
    acknowlegementsButton = (Button) findViewById(R.id.dictionaryAcknowledgementsButton);

    
    
    ButtonListener bl = new ButtonListener();
    clearButton.setOnClickListener(bl);
    returnButton.setOnClickListener(bl);
    acknowlegementsButton.setOnClickListener(bl);
    editText.addTextChangedListener(new EditTextListener());
    	
	Intent intent = new Intent(this,Dictionary_Service.class);
	startService(intent);
	bindService(intent, dConnection, Context.BIND_AUTO_CREATE);

    
    }
    
    protected void onStart() {
    	super.onStart();
	
	}

    private ServiceConnection dConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			
			dBound=false;
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			DictionaryBinder binder = (DictionaryBinder) service;
			dService = binder.getService();
			dBound = true;
			
		}
		
		
	};
    
    protected void onResume(){
    	
    	super.onResume();

    	
    	
    	
    }
    
    protected void onPause(){
    	super.onPause();
    	
    }
    
    protected void onStop(){
    	
    	super.onStop();
    	
    	
    }
    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			Intent i = new Intent();
			int id = v.getId();
			if (id == R.id.dictionaryClearButton) {
				displayedWords="";
				displayedText.setText(displayedWords);
				editText.setText("");
				dService.clearWordlist();
			} else if (id == R.id.dictionaryReturnButton) {
				finish();
			} else if (id == R.id.dictionaryAcknowledgementsButton) {
				i.setClass(Dictionary.this, Dictionary_Acknowledgements.class);
				startActivity(i);
			} else {
			}

		}
    }
    protected void onDestroy(){
    	super.onDestroy();
    	
    	if(dBound){
    		dService.clearWordlist();
    		unbindService(dConnection);
    		dBound = false;
    	}
    	
    	 
    }
    
    class EditTextListener implements TextWatcher{


		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			String word=s.toString();
			if(word.length()>=3){
				ArrayList<String> subwords = findSubwords(s.toString());
				String subword;
				for(Iterator<String> i = subwords.iterator();i.hasNext();){
					 subword=i.next();
					 if(!dService.inWordlist(subword) && dService.inDictionary(subword)){
						
					 	displayedWords+=subword+"  ";
						 displayedText.setText(displayedWords);
						 tg.startTone(ToneGenerator.TONE_PROP_BEEP);
						 dService.putWordlist(subword);
					 }
				}
			}
		}
		
		ArrayList<String> findSubwords(String word){
			ArrayList<String> result = new ArrayList<String>();
			for(int i=3; i <= word.length();i++){
				result.add(word.substring(0,i));
			}
			return result;
		}
	
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			
		}

    }
    

}


