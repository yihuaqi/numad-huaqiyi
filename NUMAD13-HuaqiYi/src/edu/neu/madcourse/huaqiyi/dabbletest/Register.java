
package edu.neu.madcourse.huaqiyi.dabbletest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.Dabble;
import edu.neu.madcourse.huaqiyi.dabbletest.HighScore.AsyncGetHighScore;
import edu.neu.madcourse.huaqiyi.dabbletest.TestMainMenu.ButtonListener;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mobileClass.*;
import edu.neu.mhealth.api.*;



public class Register extends Activity{

	Button backButton;
	Button addButton;
	Button clearButton;
	EditText nameEditText;
	SimpleAdapter listItemAdapter;
	ListView list;
	String TAG = "Dabble Register Test";

			
	ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();  
    @Override
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.dabble_test_register);  

        list = (ListView) findViewById(R.id.DabbleRegisterListView);  
        backButton = (Button) findViewById(R.id.DabbleRegisterBackButton);
        addButton = (Button) findViewById(R.id.DabbleRegisterAddButton);
        clearButton = (Button) findViewById(R.id.DabbleRegisterClearButton);
        
        nameEditText = (EditText) findViewById(R.id.DabbleRegisterEnterName);
        ButtonListener bl = new ButtonListener();
        backButton.setOnClickListener(bl);
        addButton.setOnClickListener(bl);
        clearButton.setOnClickListener(bl);

        listItemAdapter = new SimpleAdapter(this, 
                                                    listItem,
                                                    R.layout.dabble_test_registerlistitem,
                                                    new String[] {Constants.PLAYERNAMEKEY},   
                                                    new int[] {R.id.ItemPlayerName});  

        list.setAdapter(listItemAdapter);
        asyncGetPlayer();
    }  

    private void asyncGetPlayer() {
		AsyncTask<String, String,String> getPlayerTask = new AsyncGetPlayer();
		getPlayerTask.execute(Constants.PLAYERLIST);
		
	}

	class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.DabbleRegisterBackButton) {
				finish();
			} else if (id == R.id.DabbleRegisterAddButton) {
				asyncAddPlayer(nameEditText.getText().toString());
				Log.d(TAG, "Add Player:"+nameEditText.getText().toString());
			} else if (id == R.id.DabbleRegisterClearButton) {
				asyncClearPlayer();
				Log.d(TAG, "Clear All Players");
			} else {
			}
			
		}




    	
    }

	
	public void asyncClearPlayer() {
		clearPlayer();
		AsyncTask<String,String,String> clearHighScoreTask = new AsyncClearPlayer();
		clearHighScoreTask.execute(Constants.PLAYERLIST);
		
	}

	private void clearPlayer() {
		listItem.clear();
		listItemAdapter.notifyDataSetChanged();
		
	}

	public void asyncAddPlayer(String name) {
		AsyncTask<String,String,String> addPlayerTask = new AsyncAddPlayer();
		addPlayer(name); 
		
		addPlayerTask.execute(Constants.PLAYERLIST,name);
		
	}

	private void addPlayer(String name) {
        HashMap<String, String> map = new HashMap<String, String>();  
        map.put(Constants.PLAYERNAMEKEY, name);  
          
        listItem.add(map);  
        
        listItemAdapter.notifyDataSetChanged();
		
	}

    public class AsyncAddPlayer extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String players = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, arg0[0]);
    		String result;
    		String playerName = arg0[1];
    		if(players.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			result = playerName;
    		} else{
    			result = Constants.appendPlayer(players, playerName);
    		}
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, arg0[0],result);
    		Log.d(TAG,"KeyValueAPI put "+arg0[0]+" "+arg0[1]);
    		return result;
    	}
    	


    }




    public class AsyncGetPlayer extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {

    		String result = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, arg0[0]);
    		Log.d(TAG,"KeyValueAPI get "+result);
    		return result;
    	}
    	
    	protected void  onPostExecute(String result){
    		
    		
    		
    		try{
    			
    			if(result.equals("Error: No Such Key")){
    				listItem.clear();
    			} else {
    				Log.d(TAG,"KeyValueAPI get "+result);
    				String[] resultRecord = Constants.getPlayerList(result);
    				for(int i = 0; i < resultRecord.length; i++){
    			        HashMap<String, String> map = new HashMap<String, String>();  
    			        map.put(Constants.PLAYERNAMEKEY, resultRecord[i]);  
    			        listItem.add(map);  
    			        
    				}
    		
    				listItemAdapter.notifyDataSetChanged();
    			}
    		} catch (Exception e){
    			Toast.makeText(getApplicationContext(), "FAIL TO GET PlayerList", Toast.LENGTH_SHORT).show();
    		}
    	}

    }
    

    
    public class AsyncClearPlayer extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {

    		String result = KeyValueAPI.clearKey(Constants.TEAMNAME, Constants.PASSWORD, arg0[0]);
    		Log.d(TAG,"KeyValueAPI Clear "+arg0[0]);
    		return result;
    	}
    	


    }

}
