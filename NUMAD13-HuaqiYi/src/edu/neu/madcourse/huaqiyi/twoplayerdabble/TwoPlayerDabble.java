/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
***/
package edu.neu.madcourse.huaqiyi.twoplayerdabble;

import edu.neu.madcourse.huaqiyi.R;


import android.app.Activity;

import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TwoPlayerDabble extends Activity implements OnClickListener {
   private static final String TAG = "Two Player Dabble";
   TextView loginStatus;
   Button loginButton;
   Button registerButton;
   Button logoutButton;
   Button startNewGameButton;
   Button joinNewGameButton;
   Button highscoresButton;
   Button rulesButton;
   Button acknowledgementButton;
   Button quitButton;
   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Log.d(TAG,"Dabble onCreate");
      setContentView(R.layout.two_player_dabble_main);

      // Set up click listeners for all the buttons
      
      loginStatus = (TextView) findViewById(R.id.two_player_main_login_status_text);
      loginButton = (Button) findViewById(R.id.two_player_main_login_Button);
      registerButton = (Button) findViewById(R.id.two_player_main_register_Button);
      logoutButton = (Button) findViewById(R.id.two_player_main_logout_Button);
      startNewGameButton = (Button) findViewById(R.id.two_player_main_start_new_game_Button);
      joinNewGameButton = (Button) findViewById(R.id.two_player_main_join_current_gameButton);
      highscoresButton = (Button) findViewById(R.id.two_player_main_highscore_Button);
      rulesButton = (Button) findViewById(R.id.two_player_main_rules_Button);
      acknowledgementButton = (Button) findViewById(R.id.two_player_main_acknowledge_Button);
      quitButton = (Button) findViewById(R.id.two_player_main_quit_Button);
      
      registerButton.setOnClickListener(this);
      loginButton.setOnClickListener(this);
      logoutButton.setOnClickListener(this);
      startNewGameButton.setOnClickListener(this);
      joinNewGameButton.setOnClickListener(this);
      highscoresButton.setOnClickListener(this);
      rulesButton.setOnClickListener(this);
      acknowledgementButton.setOnClickListener(this);
      quitButton.setOnClickListener(this);
	  Intent i = new Intent();
	  i.setClass(this, Notification_Service.class);
      startService(i);
      Log.d("TWO PLAYER NOTIFICATION", "START NORTIFICATION SERVICE by "+Constants.getPlayer());
   }

   @Override
   protected void onResume() {
      super.onResume();
      updateLayoutByLogIn(Constants.getLogInStatus());
      Music.play(this, R.raw.dabble_background);
      Log.d(TAG, "Current player is "+Constants.getPlayer());

      
      
   }
   
   void updateLayoutByLogIn(boolean isLoggedIn){
	   if(isLoggedIn==true){
    	  loginButton.setVisibility(View.GONE);
    	  logoutButton.setVisibility(View.VISIBLE);
    	  startNewGameButton.setVisibility(View.VISIBLE);
    	  joinNewGameButton.setVisibility(View.VISIBLE);
    	  registerButton.setVisibility(View.GONE);
    	  loginStatus.setText("Welcome back, "+Constants.getPlayer());
      } else {
    	  loginButton.setVisibility(View.VISIBLE);
    	  logoutButton.setVisibility(View.GONE);
    	  startNewGameButton.setVisibility(View.GONE);
    	  joinNewGameButton.setVisibility(View.GONE);
    	  registerButton.setVisibility(View.VISIBLE);
    	  loginStatus.setText("Please Log In");
      } 
   }

   @Override
   protected void onPause() {
      super.onPause();
      Music.stop(this);
   }

   public void onClick(View v) {
	   Intent i;
      int id = v.getId();
	if (id == R.id.two_player_main_login_Button) {
		i = new Intent(this, LogIn.class);
		startActivity(i);
	} else if (id == R.id.two_player_main_logout_Button) {
		Constants.setLogInStatus(false);
		Constants.setPlayer(null);
		updateLayoutByLogIn(Constants.getLogInStatus());
	} else if (id == R.id.two_player_main_register_Button) {
		i = new Intent(this, Register.class);
		startActivity(i);
	} else if (id == R.id.two_player_main_start_new_game_Button) {
		i = new Intent(this, CreateGame.class);
		startActivity(i);
	} else if (id == R.id.two_player_main_join_current_gameButton) {
		i = new Intent(this, JoinGame.class);
		startActivity(i);
	} else if (id == R.id.two_player_main_highscore_Button) {
		i = new Intent(this,HighScore.class);
		startActivity(i);
	} else if (id == R.id.two_player_main_rules_Button) {
		i = new Intent(this, Rules.class);
		startActivity(i);
	} else if (id == R.id.two_player_main_acknowledge_Button) {
		i = new Intent(this, Acknowledgements.class);
		startActivity(i);
	} else if (id == R.id.two_player_main_quit_Button) {
		finish();
	}
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      super.onCreateOptionsMenu(menu);
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.dabble_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int itemId = item.getItemId();
	if (itemId == R.id.dabble_settings) {
		startActivity(new Intent(this, Prefs.class));
		return true;
      // More items go here (if any) ...
	}
      return false;
   }

}