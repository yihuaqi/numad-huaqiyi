package edu.neu.madcourse.huaqiyi.dabbletest;

import java.util.HashMap;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import edu.neu.madcourse.huaqiyi.R;

import edu.neu.madcourse.huaqiyi.dabbletest.Register.AsyncAddPlayer;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mhealth.api.KeyValueAPI;
import edu.neu.mobileClass.*;

public class NotificationTest extends Activity {

	Button sendButton;
	Button loginButton;
	Button quitButton;
	EditText senderEditText;
	EditText receiverEditText;
	EditText messageEditText;

	String TAG = "Dabble notification";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.dabble_test_notification);
        
        //PhoneCheckAPI.doAuthorization(this);
        sendButton = (Button) findViewById(R.id.dabble_notification_sendButton);
        loginButton= (Button) findViewById(R.id.dabble_notification_loginButton);
        quitButton = (Button) findViewById(R.id.dabble_notification_quitButton);
        senderEditText = (EditText) findViewById(R.id.dabble_notification_senderEditText);
        receiverEditText = (EditText) findViewById(R.id.dabble_notification_receiverEditText);
        messageEditText = (EditText) findViewById(R.id.dabble_notification_messageEditText);
        
        ButtonListener bl = new ButtonListener();
        sendButton.setOnClickListener(bl);
        quitButton.setOnClickListener(bl);
        loginButton.setOnClickListener(bl);


    }	

    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {
			

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.dabble_notification_sendButton) {
				String sender = senderEditText.getText().toString();
				String receiver = receiverEditText.getText().toString();
				String message = messageEditText.getText().toString();
				asyncSendMessage(sender,receiver,message);
			} else if (id == R.id.dabble_notification_quitButton) {
				finish();
			} else if (id == R.id.dabble_notification_loginButton) {
				Constants.setPlayer(senderEditText.getText().toString());
				Log.d(TAG,"setPlayer to "+senderEditText.getText().toString()+" Now, player is "+Constants.getPlayer());
			} else {
			}
			
		}
    }

	public void asyncSendMessage(String sender,String receiver,String message) {
		AsyncTask<String,String,String> addMessageTask = new AsyncSendMessage();
		
		addMessageTask.execute(sender,receiver,message);
		
	}

	

    public class AsyncSendMessage extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String sender = arg0[0];
    		String receiver = arg0[1];
    		String message = arg0[2];
    		String queryNID = Constants.queryNotificationId(receiver);
    		String NID = Constants.makeNotificationId(sender);
    		//Constants.queryNotificationId(receiver);
    		//Constants.makeNotificationId(sender);
    		//Constants.getNotification(nID,m);
    		String notifications = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, queryNID);
    		//Constants.appendNotification(notifications,Constants.makeNotificationId(sender));
    		//Constants.getNotificationList(notifications);
    		if(notifications.equals(Constants.EXCEPTIONNOSUCHKEY)||notifications.equals(Constants.EMPTYRECORD)){
    			KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, queryNID,NID);
    			Log.d(TAG, " PUT "+NID+" by key "+queryNID);
    			
    		} else{
    			notifications = Constants.appendNotification(notifications,NID);
    			KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,queryNID,notifications);
    			Log.d(TAG, " PUT "+notifications+" by key "+queryNID);
    		}
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, NID,message);		
    		Log.d(TAG, " PUT "+message+" by key "+NID);
    		
    		return null;
    	}
    }
}
