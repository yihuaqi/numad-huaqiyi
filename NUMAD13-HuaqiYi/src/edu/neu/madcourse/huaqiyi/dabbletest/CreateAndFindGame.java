
package edu.neu.madcourse.huaqiyi.dabbletest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.Dabble;
import edu.neu.madcourse.huaqiyi.dabbletest.TestMainMenu.ButtonListener;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mobileClass.*;
import edu.neu.mhealth.api.*;



public class CreateAndFindGame extends Activity{

	Button backButton;
	Button inviteButton;
	Button clearButton;
	Button getActiveGameButton;
	EditText opponentNameEditText;
	EditText playerNameEditText;
	SimpleAdapter listItemAdapter;
	ListView list;
	String TAG = "Dabble Create And Find Test";

			
	ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();  
    @Override
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.dabble_test_createfindgame);  

        list = (ListView) findViewById(R.id.DabbleCreateFindGameListView);  
        backButton = (Button) findViewById(R.id.DabbleCreateFindGameBackButton);
        inviteButton = (Button) findViewById(R.id.DabbleCreateFindGameInviteButton);
        clearButton = (Button) findViewById(R.id.DabbleCreateFindGameClearButton);
        getActiveGameButton = (Button) findViewById(R.id.DabbleCreateFindGameActiveGameButton);
        opponentNameEditText = (EditText) findViewById(R.id.DabbleCreateFindGameOpponentName);
        playerNameEditText = (EditText) findViewById(R.id.DabbleCreateFindGamePlayerName);
        ButtonListener bl = new ButtonListener();
        backButton.setOnClickListener(bl);
        inviteButton.setOnClickListener(bl);
        clearButton.setOnClickListener(bl);
        getActiveGameButton.setOnClickListener(bl);
        listItemAdapter = new SimpleAdapter(this, 
                                                    listItem,
                                                    R.layout.dabble_test_createfindgamelistitem,
                                                    new String[] {Constants.OPPONENTNAMEKEY, Constants.GAMEIDKEY},   
                                                    new int[] {R.id.ItemOpponentName,R.id.ItemGameId});  

        list.setAdapter(listItemAdapter);

    }  

    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.DabbleCreateFindGameBackButton) {
				finish();
			} else if (id == R.id.DabbleCreateFindGameInviteButton) {
				asyncInvitePlayer(playerNameEditText.getText().toString(),opponentNameEditText.getText().toString());
			} else if (id == R.id.DabbleCreateFindGameClearButton) {
				asyncClear();
			} else if (id == R.id.DabbleCreateFindGameActiveGameButton) {
				asyncGetActiveGame(playerNameEditText.getText().toString());
			} else {
			}
			
		}


    	
    }
	void clearGame() {
		listItem.clear();
		listItemAdapter.notifyDataSetChanged();
		
	}
	
	void asyncClear(){
		clearGame();
		AsyncTask<String,String,String> clearCreateFindGameTask = new AsynClearKey();
		clearCreateFindGameTask.execute();
		
	}
	
    
    public class AsynClearKey extends AsyncTask<String, String,String>{


    	protected String doInBackground(String... arg0) {

    		String players = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.PLAYERLIST);
    		String[] playerList = Constants.getPlayerList(players);
    		for(int i = 0; i < playerList.length;i++){
    			clearPlayerGame(playerList[i]);
    		}

    		return null;
    	}
    	
    	void clearPlayerGame(String playerName){
    		Log.d(TAG,"Clear Player Records:"+playerName+Constants.queryPlayerGameList(playerName) );
    		KeyValueAPI.clearKey(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(playerName));
    		
    	}


    }
	




	void asyncInvitePlayer(String playerName, String opponentName){
		AsyncTask<String,String,String> invitePlayerTask = new AsyncInvite();
		String timeStamp = System.currentTimeMillis()+"";
		String gameName = Constants.makeGameName(timeStamp,playerName,opponentName);
		
		invitePlayerTask.execute(playerName,opponentName,gameName);
	}
    
    public class AsyncInvite extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String playerName = arg0[0];
    		String opponentName = arg0[1];
    		String gameName = arg0[2];
    		String playerGames = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(playerName));
    		String playerGamesResult;
    		if(playerGames.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			playerGamesResult = gameName;
    			
    		} else {
    			 playerGamesResult = Constants.appendGame(playerGames,gameName);
    		}
    		Log.d(TAG,"PUT "+Constants.queryPlayerGameList(arg0[0])+" "+playerGamesResult);
    		String opponentGames = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(opponentName));
    		String opponentGamesResult;
    		if(opponentGames.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			opponentGamesResult =gameName;
    		} else {
    			opponentGamesResult = Constants.appendGame(opponentGames, gameName);
    		}
    		Log.d(TAG,"PUT "+Constants.queryPlayerGameList(arg0[1])+" "+opponentGamesResult);
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(playerName),playerGamesResult);
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(opponentName),opponentGamesResult);
    		
    		return null;
    	}
    	


    }
	void asyncGetActiveGame(String playerName){
		AsyncTask<String, String,String> getCreateFindGameTask = new AsyncGetActiveGame();
		getCreateFindGameTask.execute(playerName);


	}
    public class AsyncGetActiveGame extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {

    		String result = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(arg0[0]));
    		Log.d(TAG,"GET "+Constants.queryPlayerGameList(arg0[0])+" "+result);

    		return result;

    		
    	}
    	
    	protected void  onPostExecute(String result){
    		

    		
    	
    		listItem.clear();
			if(result.equals(Constants.EXCEPTIONNOSUCHKEY)){
				
			} else {
				
	    		String[] gameList = Constants.getGameList(result);
	    		for(int i = 0 ; i < gameList.length; i++){
	    			String gameName = gameList[i];
	    			String[] gameAttribute = gameName.split(Constants.ATTRIBUTESEPERATOR);
			        HashMap<String, String> map = new HashMap<String, String>();  
			        map.put(Constants.OPPONENTNAMEKEY, gameAttribute[0]+" vs "+gameAttribute[1]);  
			        map.put(Constants.GAMEIDKEY, gameAttribute[2]);  
			        listItem.add(map);  
	    			
	    		}	
				listItemAdapter.notifyDataSetChanged();
			}
		}
    	

    }



}
