package edu.neu.madcourse.huaqiyi.twoplayerdabble;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.Prefs;
import edu.neu.mhealth.api.KeyValueAPI;

public class MultiGame extends Activity {
	
	   private static final String TAG = "Two Player Dabble";
	public static final int NORMAL_UPDATE = 6;
	public static final int YOU_WIN = 3;
	public static final int YOU_LOSE = 2;
	public static final int INITGAME = 7;
	public static final int IS_NOT_YOUR_TURN=4;
	public static final int IS_YOUR_TURN = 5;
	   private ArrayList<Letter> letters = new ArrayList<Letter>();
	   public String solution;
	   public String boardWord;
	   public SoundPool soundPool;
	   public int DABBLE_SELECT;
	   public int DABBLE_VALID;
	   public int DABBLE_ALERT;
	   public int DABBLE_WIN;
	   public int DABBLE_LOSE;
	   static int numRoot[] = new int[]{45,87,133,182};
	   Button pauseButton;
	   Button hintButton;
	   Button quitButton;
	   TextView scoreText;
	   TextView timeText;
	   String opponentScore="";
	   private static final int VOICE_RECOGNITION_REQUEST_CODE=1004;
	   private static final int GAME_ALERT=1;
	   private boolean destroyTimer = false;
	  private boolean gameActive = false;
	   float font_size;
	   Paint selectedPaint;
	   Paint unSelectedPaint;
	   Paint validPaint;
	   Paint menuPaint;
	   Paint hintPaint;
	   Paint timerPaint;
	   Paint timerAlertPaint;
	   Paint background;
	   private  int gameStatus;
	   public boolean TimerGo=true;
	   private final int GAME_TIME = 120000;
	   private final int GAME_TIMEALERT = 30000;
	   public boolean isPlayingAlert = false;
	   private int timeLeft;
	   private int score;
	   boolean isPlayingLose = false;
	   public boolean allowToPlayAlert = false;
	   int alertStreamID=0;
	   boolean showHint = false;
	   RelativeLayout[] tiles = new RelativeLayout[18];
	   TextView[] tilesLetter = new TextView[18];
	   TextView[] tilesScore = new TextView[18];
	   int selectedTileIndex;
	   final int NOTILEISSELECTED = -1;
	   String playerStatus="";
	   String opponentStatus="";
	   TextView playerStatusText;
	   TextView opponentStatusText;
	   ScrollView playerStatusScroll;
	   ScrollView opponentStatusScroll;
	   boolean isYourTurn = false;
	   boolean gameOver = false;
	   boolean youFirst = false;
	   boolean hasBoard = false;
	   Thread autoUpdate;
	   String[] wordList = new String[]{"","","",""};
	   String[] opponentWordList = new String[]{"","","",""};
	   String opponentWords = "";
	   String words = "";
	   private boolean useAlertPaint = false;
	   private boolean hasMicrophone = false;
	   Button microphone;

	public boolean destroyAutoUpdate=false;

	   protected void onCreate(Bundle savedInstanceState) {
		      super.onCreate(savedInstanceState);
		      youFirst = !getIntent().getBooleanExtra(Constants.HAS_BOARD,false);
		      hasBoard = getIntent().getBooleanExtra(Constants.HAS_BOARD,false);
		      isYourTurn = !getIntent().getBooleanExtra(Constants.HAS_BOARD,false);
		      Constants.setGameId(getIntent().getStringExtra(Constants.GAME_ID));
		      gameActive = true;
		      
		      timeLeft = GAME_TIME;
		      score=0;

		      allowToPlayAlert = true;
		      selectedTileIndex = NOTILEISSELECTED;
		      soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC,5);
		      //TODO find music
		      DABBLE_SELECT = soundPool.load(this,R.raw.dabble_select, 1);
		      DABBLE_VALID = soundPool.load(this,R.raw.dabble_valid, 1);
		      DABBLE_ALERT = soundPool.load(this,R.raw.dabble_alert, 1);
		      DABBLE_WIN = soundPool.load(this,R.raw.dabble_win, 1);
		      DABBLE_LOSE = soundPool.load(this,R.raw.dabble_lose, 1);
		      
		      
		      setContentView(R.layout.dabble_test_multi);
		      

		      assignValue();
		      
		      
		      
		      if(!hasBoard){
		    	  Log.d(TAG, "INITIAL Board locally");
			      iniGame();
			      iniLetters();  
			      hasBoard = true;
		      } else {
		    	  Log.d(TAG, "INITIAL Board remotely");

		      }
		      new AsyncGame().execute();
		      
		      // ...
		      // If the activity is restarted, do a continue next time
		      //getIntent().putExtra(KEY_DIFFICULTY, DIFFICULTY_CONTINUE);
		   }

	   @Override
	protected void onDestroy() {
		gameActive = false;
		new HighScore().asyncAddHighScore(Constants.getPlayer(), score+"");
		super.onDestroy();
	}

	private void assignValue() {
		int tileId = R.id.multiTile1;
		int letterId = R.id.multiLetter1;
		int scoreId = R.id.multiScore1;
		TileListener tl = new TileListener();
		ButtonListener bl = new ButtonListener();
		for(int i = 0; i < 18; i++){

				tiles[i] = (RelativeLayout) findViewById(tileId);
				tiles[i].setOnClickListener(tl);
				tilesLetter[i] = (TextView) findViewById(letterId);
				tilesScore[i] = (TextView) findViewById(scoreId);
				tileId+=3;
				letterId+=3;
				scoreId+=3;
				
			}
		scoreText = (TextView) findViewById(R.id.dabble_test_multi_score);
		timeText = (TextView) findViewById(R.id.dabble_test_multi_time);
		pauseButton = (Button)findViewById(R.id.multiFinishButton);
		pauseButton.setOnClickListener(bl);
		hintButton = (Button)findViewById(R.id.multiHintButton);
		hintButton.setOnClickListener(bl);
		quitButton = (Button)findViewById(R.id.multiQuitButton);
		quitButton.setOnClickListener(bl);
		playerStatusText = (TextView) findViewById(R.id.multiYourStatusText);
		//playerStatusText.setMovementMethod(new ScrollingMovementMethod());
		opponentStatusText = (TextView) findViewById(R.id.multiOpponentStatusText);
		//opponentStatusText.setMovementMethod(new ScrollingMovementMethod());
		playerStatusScroll = (ScrollView) findViewById(R.id.multiYourStatusScroll);
		opponentStatusScroll = (ScrollView) findViewById(R.id.multiOpponentStatusScroll);
		if(isYourTurn){
			pauseButton.setVisibility(View.VISIBLE);
		} else {
			pauseButton.setVisibility(View.GONE);
		}
	      microphone = (Button)findViewById(R.id.recorder);

	      
	      PackageManager pm = getPackageManager();
	      List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
	      if(activities.size()!=0){
		      MicrophoneListener ml = new MicrophoneListener();
		      microphone.setOnClickListener(ml);
		      hasMicrophone = true;
	      } else {
	    	  microphone.setVisibility(View.GONE);
	    	  addPlayerMessage("Install Voice Search package to be able to use Audio Input\n");
	    	  hasMicrophone = false;
	    	  
	      }
	      if(!isYourTurn){
	    	  microphone.setVisibility(View.GONE);
	    	  
	      }
	   }
	   
	   class MicrophoneListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			int id = v.getId();
			if (id == R.id.recorder) {
				audioInput();
			}
			
		}

		private void audioInput() {
			Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Please say a word");
			startActivityForResult(intent,VOICE_RECOGNITION_REQUEST_CODE );
			
		}
		

		   
	   }
	   
		protected void onActivityResult(int requestCode, int resultCode, Intent data){
			if(resultCode == RESULT_OK){
				if(requestCode == VOICE_RECOGNITION_REQUEST_CODE){
					ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
					wordsFilter(matches);
					
					for(int i = 0; i < matches.size(); i++){
						Log.d(TAG, "Recognized word: "+matches.get(i));
					}
					adaptWordsFromAudio(matches);
				} 
				
			}
			super.onActivityResult(requestCode, resultCode, data);
		}
		
		private void adaptWordsFromAudio(ArrayList<String> matches) {
			
			boolean done = false;
			Iterator<String> i = matches.iterator();
			while(!done && i.hasNext()){
				String word = i.next().toLowerCase();
				done = adaptWordFromAudio(word,word.length());
			}
			
		}
		private boolean adaptWordFromAudio(String word,int length) {
			boolean canBeDone = checkCanBeDone(word, length);
			if(canBeDone){
				Log.d(TAG,word+" can be done!");
				switch(length){
				case 3:
					for(int i = 0; i < 3; i ++){
						boolean done = false;
						for(int j = 0; j < 18 && !done; j++){
							if(j>i || j<0){
								if(!letters.get(j).isValid&&letters.get(j).getLetter().charAt(0)==(word.charAt(i))){
									swapLetter(letters.get(i), letters.get(j));
									done = true;
								}
							}
						}
					}
					break;
				case 4:
					for(int i = 3; i < 7; i ++){
						boolean done = false;
						for(int j = 0; j < 18 && !done; j++){
							if(j>i || j<3){
								if(!letters.get(j).isValid&&letters.get(j).getLetter().charAt(0)==(word.charAt(i-3))){
									swapLetter(letters.get(i), letters.get(j));
									done = true;
								}
							}
						}
					}
					break;
				case 5:
					for(int i = 7; i < 12; i ++){
						boolean done = false;
						for(int j = 0; j < 18 && !done; j++){
							if(j>i || j<7){
								if(!letters.get(j).isValid&&letters.get(j).getLetter().charAt(0)==(word.charAt(i-7))){
									swapLetter(letters.get(i), letters.get(j));
									done = true;
								}
							}
						}
					}
					break;
				case 6:
					for(int i = 12; i < 18; i ++){
						boolean done = false;
						for(int j = 0; j < 18 && !done; j++){
							if(j>i || j<12){
								if(!letters.get(j).isValid&&letters.get(j).getLetter().charAt(0)==(word.charAt(i-12))){
									swapLetter(letters.get(i), letters.get(j));
									done = true;
								}
							}
						}
					}
					break;
				}
				checkValidWords();
			}
			return canBeDone;
		}
		
		private boolean checkCanBeDone(String word, int length){
			int bracket[] = new int[26];
			for(int i = 0; i < 18; i++){
				bracket[i]=0;
			}
			
			for(int i = 0; i < 18; i++){
				Letter l = letters.get(i);
				if(!l.isValid){
					
					bracket[(l.getLetter().charAt(0)-'a')]++;
				}
			}
			for(int i = 0; i < length; i++){
				if((word.charAt(i)-'a')>25 || (word.charAt(i)-'a')<0){
					return false;
				} else {
					bracket[(word.charAt(i)-'a')]--;
				}
			}
			
			for(int i = 0; i < 26; i++){
				if(bracket[i]<0){
					return false;
				}
			}
			return true;
		}
		void wordsFilter(ArrayList<String> words){
			ArrayList<String> temp = (ArrayList<String>) words.clone();
			words.clear();
			for(Iterator<String> i = temp.iterator(); i.hasNext();){
				String word = i.next().split(" ")[0];
				if(word.length()<=6 && word.length()>=3){
					words.add(word.toLowerCase());
				}
			}
			
		}
		
	   class TileListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			if(isYourTurn){
				int tileId = R.id.multiTile1;
				int viewId = v.getId();
				int index = (viewId - tileId)/3;
				if(selectedTileIndex == NOTILEISSELECTED){
					selectedTileIndex = index;
					letters.get(index).setSelected();
				} else {
					if(index == selectedTileIndex){
						letters.get(index).setUnSelected();
						selectedTileIndex = NOTILEISSELECTED;
					} else {
						swapLetter(letters.get(index),letters.get(selectedTileIndex));
						checkValidWords();
					}
					
				}
				
			}
		}
		   
	   }
		private void swapLetter(Letter firstL, Letter secondL) {
			//firstL.setUnSelected();
			//secondL.setUnSelected();
			   String firstScore = firstL.getScore();
			   String secondScore = secondL.getScore();
			   String firstLetter = firstL.getLetter();
			   String secondLetter = secondL.getLetter();
			   
			   firstL.setLetter(secondLetter);
			   firstL.setScore(secondScore);
			   
			   secondL.setLetter(firstLetter);
			   secondL.setScore(firstScore);
			   firstL.setUnSelected();
			   secondL.setUnSelected();
			   selectedTileIndex = NOTILEISSELECTED;
			   addAndDisplayPlayerMessage(" Swapped "+firstLetter+" with "+secondLetter+"\n");
			   
			// TODO Auto-generated method stub
			   // Also change the score....

			   
			
		}
	   private void updateBoardWord() {
		   String result="";
			for(int i = 0; i < 18 ; i++){
				result+=letters.get(i).getLetter();
			}
			boardWord = result;
		}

	class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			int tileId = R.id.multiTile1;
			int viewId = v.getId();
			int id = v.getId();
			if (id == R.id.multiFinishButton) {
				finishTurn();
			} else if (id == R.id.multiHintButton) {
				setShowHint(!showHint);
			} else if (id == R.id.multiQuitButton) {
				destroyTimer=true;
				soundPool.stop(alertStreamID);
				isPlayingAlert = false;
				allowToPlayAlert =false;
				destroyAutoUpdate=true;
				Quit();
			}
			
		}

		
		   
	   }
	   
	   private void checkValidWords() {
		    String firstRow="";
			String secondRow="";
			String thirdRow="";
			String fourthRow="";
			for(int i = 0 ; i < 3 ; i++){
				firstRow += letters.get(i).getLetter();
				
			}

			
			for(int i = 3 ; i < 7 ; i++){
				secondRow += letters.get(i).getLetter();
				
			}

			
			for(int i = 7 ; i < 12 ; i++){
				thirdRow += letters.get(i).getLetter();
				
			}

			
			for(int i = 12 ; i < 18 ; i++){
				fourthRow += letters.get(i).getLetter();
				
			}

			boolean[] isValid = new boolean[]{false,false,false,false};
			isValid[0] = inDictionary(firstRow.toLowerCase())&&(!InWordList(opponentWordList,firstRow));
			isValid[1] = inDictionary(secondRow.toLowerCase())&&(!InWordList(opponentWordList,secondRow));
			isValid[2] = inDictionary(thirdRow.toLowerCase())&&(!InWordList(opponentWordList,thirdRow));
			isValid[3] = inDictionary(fourthRow.toLowerCase())&&(!InWordList(opponentWordList,fourthRow));
			if((isValid[0] && !InWordList(wordList,firstRow)) || (isValid[1] && !InWordList(wordList,secondRow)) 
					|| (isValid[2]&&!InWordList(wordList,thirdRow)) || (isValid[3]&&!InWordList(wordList,fourthRow)) ){

					soundPool.play(DABBLE_VALID, 1.0f, 1.0f, 0, 0, 1);

				
			}
			
			if(isValid[0]  ){
				if(!InWordList(wordList,firstRow) && isYourTurn){
					addAndDisplayPlayerMessage(" Spelt a valid word "+firstRow+"!"+"\n");
				} else if(!InWordList(wordList,firstRow) && !isYourTurn){
					addAndDisplayOpponentMessage(" Spelt a valid word "+firstRow+"!"+"\n");
				}
				wordList[0] = firstRow;
			} else {
				wordList[0]=" ";
			}
			if(isValid[1]){
				if(!InWordList(wordList,secondRow) && isYourTurn){
					addAndDisplayPlayerMessage(": Spelt a valid word "+secondRow+"!"+"\n");
				}else if(!InWordList(wordList,secondRow) && !isYourTurn){
					addAndDisplayOpponentMessage(" Spelt a valid word "+secondRow+"!"+"\n");
				}
				wordList[1] = secondRow;
			} else {
				wordList[1]=" ";
			}
			if(isValid[2]){
				if(!InWordList(wordList,thirdRow) && isYourTurn){
					addAndDisplayPlayerMessage(": Spelt a valid word "+thirdRow+"!"+"\n");
				}else if(!InWordList(wordList,thirdRow) && !isYourTurn){
					addAndDisplayOpponentMessage(" Spelt a valid word "+thirdRow+"!"+"\n");
				}
				wordList[2]=thirdRow;
			} else {

				wordList[2] = " ";
			}
			if(isValid[3]){
				if(!InWordList(wordList,fourthRow) && isYourTurn){
					addAndDisplayPlayerMessage(": Spelt a valid word "+fourthRow+"!"+"\n");
				}else if(!InWordList(wordList,fourthRow) && !isYourTurn){
					addAndDisplayOpponentMessage(" Spelt a valid word "+fourthRow+"!"+"\n");
				}
				wordList[3] = fourthRow;
			} else {
				wordList[3]=" ";
			}
			 
			for(int i = 0 ; i < 3 ; i++){
				
				letters.get(i).setValid(isValid[0]&&!InWordList(opponentWordList,firstRow));
				

			}
		
			
			

			for(int i = 3 ; i < 7 ; i++){
				letters.get(i).setValid(isValid[1]&&!InWordList(opponentWordList,secondRow));
				//Log.d(TAG, secondRow+" First word is Valid");


			}
			
			
			

			for(int i = 7 ; i < 12 ; i++){
				letters.get(i).setValid(isValid[2]&&!InWordList(opponentWordList,thirdRow));
				//Log.d(TAG, thirdRow+" Third word is valid");

			}
			
			
			

			for(int i = 12 ; i < 18 ; i++){
				letters.get(i).setValid(isValid[3]&&!InWordList(opponentWordList,fourthRow));
				//Log.d(TAG, fourthRow + " Fourth word is valid");


			}
			score = 0;
			for(int i = 0; i < 4; i++){
				if(isValid[i]){
					score += getScoreInRow(i);
				}
			}
			//checkWin(isValid);
		}
	   
		private void addAndDisplayOpponentMessage(String message) {
			   addOpponentMessage(getTimeShort()+"\n"+message);
			   opponentStatusText.setText(opponentStatus);
			   opponentStatusScroll.post(new Runnable(){
				   public void run(){
					   opponentStatusScroll.fullScroll(View.FOCUS_DOWN);
				   }
			   });
		
	}

		private boolean InWordList(String[] wordlist, String Row) {
		return wordlist[Row.length()-3].equals(Row);
	}

		

	
	private int getScoreInRow(int i) {
		int result = 0;
		switch(i){
		case 0:
			
			for(int j = 0; j < 3; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 3;
		case 1:

			for(int j = 3; j < 7; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 4;
			
		case 2:

			for(int j = 7; j < 12; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 5;
			
		case 3:

			for(int j = 12; j < 18; j ++){
				result+= Integer.parseInt(letters.get(j).getScore());
			}
			return result * 6;
			
		}
		return 0;
	}
	private String getScoreForLetter(char charAt) {
		switch (charAt){
		case 'a':
		case 'e':
		case 'i':
		case 'l':
		case 'n':
		case 'o':
		case 'r':
		case 's':
		case 't':
		case 'u':
			return "3";
		case 'd':
		case 'g':
			return "6";
		case 'b':
		case 'c':
		case 'm':
		case 'p':
			return "9";
		case 'f':
		case 'h':
		case 'v':
		case 'w':
		case 'y':
			return "12";
		case 'k':
			return "15";
		case 'j':
		case 'x':
			return "24";
		case 'q':
		case 'z':
			return "30";
		}
		return "ERROR:"+charAt;
		
	}
	void iniGame(){

		   
		   solution = getWord(3)+getWord(4)+getWord(5)+getWord(6);
		   Log.d(TAG,"Get Solution:" + solution);
		   boardWord = randomizeWord(solution);
		   Log.d(TAG,"Get boardWord:"+boardWord);

		
	   }
	   
	   @Override
	   public boolean onCreateOptionsMenu(Menu menu) {
	      super.onCreateOptionsMenu(menu);
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.dabble_menu, menu);
	      return true;
	   }

	   @Override
	   public boolean onOptionsItemSelected(MenuItem item) {
	      int itemId = item.getItemId();
		if (itemId == R.id.dabble_settings) {
			startActivity(new Intent(this, Prefs.class));
			return true;
	      // More items go here (if any) ...
		}
	      return false;
	   }
	   protected void onResume(){
		   super.onResume();
		   //TODO add music file.
		   if(isYourTurn){
			   TimerGo=true;
		   }
		   Music.play(this, R.raw.dabble_background);
	   }
	   protected void onPause(){
		   super.onPause();
		   if(TimerGo){
			   TimerGo=false;
		   }
		   soundPool.stop(alertStreamID);
		   allowToPlayAlert = false;
		   isPlayingAlert =false;
		   
		  
		   Music.stop(this);
				   
	   }
	   private int getGameState(){
		   return gameStatus;
	   }

	   
		private String randomizeWord(String solution) {
			List<String> ls = new ArrayList<String>();
			for(int i = 0; i < solution.length(); i++){
				ls.add(String.valueOf(solution.charAt(i)));
			}
			Collections.shuffle(ls);
			StringBuffer sb = new StringBuffer();
			for(Iterator<String> it = ls.iterator(); it.hasNext();){
				sb.append(it.next());
			}
			String result = new String(sb);
			return result;
			
		}

	String getWord(int length){
		Random r= new Random(System.currentTimeMillis());
		int fileIndex = r.nextInt(numRoot[length-3]);
		int wordIndex = r.nextInt(numRoot[length-3]);
		String filename =length+"_"+fileIndex;
		int count;
		if(wordIndex==0){
			count=0;
			
		} else {
			count = r.nextInt(wordIndex);
		}
		String result="ERROR";
		try{
    		InputStream is = getAssets().open(filename);
    		InputStreamReader isReader = new InputStreamReader(is);
    		BufferedReader bReader = new BufferedReader(isReader);
    		result = null;
    		for(int i =0; i < count - 1; i++){
    			bReader.readLine();
    		}
    		result = bReader.readLine();
    		
    		
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
		return result;
	   }
	   
	   boolean inDictionary(String word){
	    	boolean result = false;
	    	String filename = word.length()+"/" +word.substring(0,2);
	    	
	    	try{
	    		InputStream is = getAssets().open(filename);
	    		InputStreamReader isReader = new InputStreamReader(is);
	    		BufferedReader bReader = new BufferedReader(isReader);
	    		String temp = null;
	    		while((temp = bReader.readLine())!=null){
	    			if(temp.equals(word)){
	    				return true;
	    			}
	    		}
	    		
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	return result;
	    }

	   
	   String getSolution(){
		   return solution;
	   }
	   
	   String getBoardWord(){
		   return boardWord;
	   }

		public void Quit() {
			if(isYourTurn){
				finishTurn();
			}
			finish();

		}
		private void finishTurn() {
			pauseButton.setVisibility(View.GONE);
			
			new Thread(new Runnable(){

				@Override
				public void run() {
					if(youFirst){
						isYourTurn = false;
						Log.d(TAG,"YOUFIRSTYOURTURN ACTION: "+timeLeft+" "+TimerGo);
						TimerGo=false;
						
						KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerScore(Constants.getPlayer()),score+"");
						
						words = Constants.makeWords(wordList);
						addPlayerMessage(Constants.getPlayer()+":"+"\n");
						addPlayerMessage("Score"+":"+score+"\n");
						for(int i = 0; i < 4; i ++)
						{
							if(wordList[i].length()>=3 ){
								addPlayerMessage("Spelt words"+":"+ wordList[i]+"\n");
							}
						}
						
						KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getPlayer()),playerStatus);
						KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameWordList(Constants.getPlayer()),words);
						KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus(),Constants.makePlayerTurn(Constants.getOpponent()));
					} else {
						TimerGo=false;
						isYourTurn = false;
						opponentScore = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerScore(Constants.getOpponent()));
						
						if (score>Integer.parseInt(opponentScore)){
							KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus(),Constants.makeGamePlayerWin(Constants.getPlayer()));
							
							
						} else {
							KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus(),Constants.makeGamePlayerWin(Constants.getOpponent()));
							
							
							

						}
					}
					
				}
				
			}).start();
			
		}
	    public class AsyncGame extends AsyncTask<Void, Integer,Void>{

	    	
			@Override
	    	protected Void doInBackground(Void... arg0) {
	    		
	    		if(youFirst){
	    			if(isYourTurn){
		    			KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus(),Constants.makePlayerTurn(Constants.getPlayer()));
		    			
		    			KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameHint(),solution);
		    		}
	    			
	    			
	    		} else {
	    			boardWord = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameBoard());

	    			solution = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameHint());
	    			
	    			
	    		}
	    		Log.d("TWO PLAYER NOTIFICATION", Constants.queryGameBoard());
	    		publishProgress(INITGAME);
	    		while(gameActive){
	    			
	    			long startTime = System.currentTimeMillis();
	    			try{
	    				Thread.sleep(300);
	    			} catch (Exception e){
	    				e.printStackTrace();
	    			}
	    			if(isYourTurn){
	    				if(youFirst){
	    					youFirstYourTurnAction();
	    				} else {
	    					notYouFirstYourTurnAction();
	    				}
	    			} else {
	    				if(youFirst){
	    					youFirstNotYourTurnAction();
	    				} else {
	    					notYouFirstNotYourTurnAction();
	    				}
	    			}
	    			generalAction();
	    			publishProgress(NORMAL_UPDATE);
	    			long endTime = System.currentTimeMillis();
	    			if(TimerGo){
	    				timeLeft -= (endTime-startTime);
	    			}
	    			
	    		}
	    		return null;
	    		
	    	}
		    private void generalAction() {
		    	KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getPlayer()),playerStatus);
		    	if(!showHint){
		    		//TODO
					//opponentStatus = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getOpponent()));
					//Log.d(TAG, "Get playerstatus by "+Constants.queryGamePlayerStatus(Constants.getOpponent())+"    "+opponentStatus);
				}
				if(timeLeft<=GAME_TIMEALERT){
					publishProgress(GAME_ALERT);
					if(!isPlayingAlert && allowToPlayAlert){
						alertStreamID=soundPool.play(DABBLE_ALERT, 1.0f, 1.0f, 0, 3, 1);
						isPlayingAlert = true;
					}
				}
				
			}
			private void notYouFirstNotYourTurnAction() {
				Log.d(TAG,"notYouFirstNotYourTurnAction put "+playerStatus+" by "+Constants.queryGamePlayerStatus(Constants.getPlayer()));
				
				TimerGo=false;
				
				boardWord = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameBoard());
				
				if(!showHint){
					//TODO
					//opponentStatus = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getOpponent()));
					//Log.d(TAG, "Get playerstatus by "+Constants.queryGamePlayerStatus(Constants.getOpponent())+"    "+opponentStatus);
				}
				
				String gs = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus());

				String[] gsRecord = Constants.getGameStatus(gs); 
				if(gsRecord[0].equals(Constants.getPlayer())&&gsRecord[1].equals(Constants.ATTRIBUTE_TURN)){
					addPlayerMessage("It's Your Turn!!");
					isYourTurn = true;
					publishProgress(IS_YOUR_TURN);
					TimerGo = true;
					words = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameWordList(Constants.getOpponent()));
	    			opponentWordList = Constants.getWordList(words);
	    			
	    			timeLeft = GAME_TIME;
				}
				//KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getPlayer()),playerStatus);
				
			}
			private void youFirstNotYourTurnAction() {
				
				TimerGo=false;
				Log.d(TAG,"youFirstNotYourTurnAction.");
				boardWord = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameBoard());
				
				if(!showHint){
					//TODO
					//String result= KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getOpponent()));
					/*
					if(result.equals(Constants.EXCEPTIONNOSUCHKEY)){
						opponentStatus = "";
					} else {
						opponentStatus= result; 
					}*/
				}


				
			}
			private void notYouFirstYourTurnAction() {
				
				Log.d(TAG,"notYouFirstYourTurnAction put"+playerStatus+" by "+Constants.queryGamePlayerStatus(Constants.getPlayer()));
				updateBoardWord();
				KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameBoard(),boardWord);
				KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getPlayer()),playerStatus);
				if(timeLeft <= 0 && TimerGo){
					TimerGo=false;
					isYourTurn = false;
					opponentScore = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerScore(Constants.getOpponent()));
					publishProgress(IS_NOT_YOUR_TURN);
					if (score>Integer.parseInt(opponentScore)){
						KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus(),Constants.makeGamePlayerWin(Constants.getPlayer()));
;
					} else {
						KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus(),Constants.makeGamePlayerWin(Constants.getOpponent()));

						

					}
					
				}
				
				//Log.d(TAG, "Put playerstatus by "+Constants.queryGamePlayerStatus(Constants.getPlayer())+"    "+result);
				
				
				//KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,"123","I'm UPLOADING!!!!!!");
				//Log.d(TAG, "Put playerstatus by "+Constants.queryGamePlayerStatus(Constants.getPlayer())+"   "+result);
				
			}
			private void youFirstYourTurnAction() {
				
				Log.d(TAG,"youFirstYourTurnAction.");
				if(timeLeft <= 0 && TimerGo){
					
					isYourTurn = false;
					//Log.d(TAG,"YOUFIRSTYOURTURN ACTION: "+timeLeft+" "+TimerGo);
					TimerGo=false;
					KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerScore(Constants.getPlayer()),score+"");
					
					words = Constants.makeWords(wordList);
					addPlayerMessage(Constants.getPlayer()+":"+"\n");
					addPlayerMessage("Score"+":"+score+"\n");
					for(int i = 0; i < 4; i ++)
					{
						if(wordList[i].length()>=3 ){
							addPlayerMessage("Spelt words"+":"+ wordList[i]+"\n");
						}
					}
					
					KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getPlayer()),playerStatus);
					KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameWordList(Constants.getPlayer()),words);
					KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameStatus(),Constants.makePlayerTurn(Constants.getOpponent()));
					
				}
				updateBoardWord();
				KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGameBoard(),boardWord);
				String result = KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,Constants.queryGamePlayerStatus(Constants.getPlayer()),playerStatus);
				//Log.d(TAG, "Put playerstatus by "+Constants.queryGamePlayerStatus(Constants.getPlayer())+"    "+result);
				
				
			}
			protected void onProgressUpdate(Integer... progress) {
				
		    	switch(progress[0]){
		    	case GAME_ALERT:
		    		timeText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
		    		break;
		    	case NORMAL_UPDATE:
		    		
			    	playerStatusText.setText(playerStatus);
					opponentStatusText.setText(opponentStatus);
					
					scoreText.setText("Score:"+score);
					if(TimerGo){
						timeText.setText("Time:"+timeLeft/1000);
					}
			    	if(!isYourTurn){
			    		adaptLetters(boardWord);
			    	}
		    		break;
		    	case INITGAME:
		    		iniLetters();  
		    		break;
		    	case IS_YOUR_TURN:
		    		pauseButton.setVisibility(View.VISIBLE);
		    		if(hasMicrophone){
		    			microphone.setVisibility(View.VISIBLE);
		    		}
		    		break;
		    	case IS_NOT_YOUR_TURN:
		    		pauseButton.setVisibility(View.GONE);
		    		microphone.setVisibility(View.GONE);
		    		break;
		    	default:
		    		break;
		    	}

		     }

	    }


	    	


		   public class Letter {
			   Boolean isSelected=false;
			   Boolean isValid = false;
		
				int index;
				RelativeLayout tile;
				TextView letter;
				TextView score;
				   Letter(){
				   }
			   
			   public void setValid(boolean valid) {
				isValid = valid;
				if(valid){
					tile.setBackgroundResource(android.R.drawable.btn_star_big_on);
				} else {
					tile.setBackgroundResource(android.R.drawable.btn_default);
					
				}
				
			}

			public void setLetter(String l) {
				   	letter.setText(l);
				
			}

			public void setIndex(int i) {
				   index = i;
				
			}



			public String getLetter(){
				return (String) letter.getText();
				   
			   }

			   public void setSelected(){
				   isSelected = true;
				   tile.setBackgroundResource(android.R.drawable.button_onoff_indicator_on);
			   }
			   
			   public void setUnSelected(){
				   isSelected = false;
				   if(isValid){
					   tile.setBackgroundResource(android.R.drawable.btn_star_big_on);
				   } else {
					   tile.setBackgroundResource(android.R.drawable.btn_default);
				   }
			   }
				Letter(RelativeLayout tiles, TextView l, TextView s, int i){
					tile = tiles;
					score = s;
					index = i;
					letter = l;
				   }



				public void setScore(String s) {
					score.setText(s);
					
				}
				
				public String getScore(){
					return score.getText().toString();
				}

				public void setLetter(char l) {
					letter.setText(Character.toString(l));
					
				}
			   
		   }
		   private void iniLetters(){
			boardWord = boardWord.toLowerCase();
			   for(int i = 0; i < 18; i++){
				   Letter temp = new Letter(tiles[i],tilesLetter[i],tilesScore[i],i);
				   temp.setLetter(boardWord.charAt(i));
				   
				   temp.setScore(getScoreForLetter(boardWord.charAt(i)));

				   letters.add(temp);
			   }
			   
			   checkValidWords();
		   }
		   
		   private void adaptLetters(String bw){
			   for(int i = 0 ; i < 18; i++){
				   letters.get(i).setLetter(bw.charAt(i));
				   letters.get(i).setScore(getScoreForLetter(bw.charAt(i)));
			   }
			   
			   
			   checkValidWords();
			   
		   }


			   private void setShowHint(boolean show){
				   if(show){
					   String hint = solution.substring(0,3)+"\n"+solution.substring(3,7)+"\n"+solution.substring(7,12)+"\n"+solution.substring(12,18)+"\n";
					   opponentStatus = hint;
					   
				   } else {
					   opponentStatus = "";
					   
				   }
				   showHint = show;
			   }
			   public static String getTimeShort() {
				   SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
				   Date currentTime = new Date();
				   String dateString = formatter.format(currentTime);
				   return dateString;
				  }
			   
			   private void addAndDisplayPlayerMessage(String message){
				   addPlayerMessage(getTimeShort()+"\n"+message);
				   playerStatusText.setText(playerStatus);
				   playerStatusScroll.post(new Runnable(){
					   public void run(){
						   playerStatusScroll.fullScroll(View.FOCUS_DOWN);
					   }
				   });
				   //playerStatusScroll.fullScroll(View.FOCUS_DOWN);
			    	
			   }
			   
			   private void addPlayerMessage(String message){
				   playerStatus+=message;
			   }
			   private void addOpponentMessage(String message){
				   opponentStatus+=message;
			   }
			   
			   
			   
}
