
package edu.neu.madcourse.huaqiyi.twoplayerdabble;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.Dabble;

import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mobileClass.*;
import edu.neu.mhealth.api.*;



public class HighScore extends Activity{


	SimpleAdapter listItemAdapter;
	ListView list;
	String TAG = "Two Player Dabble HighScore";

			
	ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();
	private Button backButton;  
    @Override
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.two_player_dabble_highscore);  

        list = (ListView) findViewById(R.id.two_player_highscoreListView);  
        backButton = (Button) findViewById(R.id.two_player_highscoreBackButton);
        ButtonListener bl = new ButtonListener();
        backButton.setOnClickListener(bl);
        

        listItemAdapter = new SimpleAdapter(this, 
                                                    listItem,
                                                    R.layout.two_player_dabble_highscore_listitem,
                                                    new String[] {Constants.PLAYERRANKKEY,Constants.PLAYERNAMEKEY, Constants.PLAYERSCOREKEY},   
                                                    new int[] {R.id.two_player_highscoreItemPlayerRank,R.id.two_player_highscoreItemPlayerName,R.id.two_player_highscoreItemPlayerScore});  

        list.setAdapter(listItemAdapter);
        asyncGetHighScore();
    }  

    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.two_player_highscoreBackButton) {
				finish();
			} else {
			}
			
		}


    	
    }

	



	void asyncAddHighScore(String name, String score){
		AsyncTask<String,String,String> addHighScoreTask = new AsyncAddHighScore();
		//addHighScore(name,score); 
		
		addHighScoreTask.execute(Constants.HIGHSCORE,name,score);
	}
	
    public class AsyncAddHighScore extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String HighScores = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.HIGHSCORE);
    		
    		String name = arg0[1];
    		String score = arg0[2];
    		String result="";
    		if(HighScores.equals(Constants.EXCEPTIONNOSUCHKEY)){
    			result = Constants.makeHighScoreRecord(name,score);
    		} else{
    			String[] highScoreList = Constants.getHighScoreList(HighScores);
    			boolean ifSmallest = true;

    			/*
    			for(int i = 1; i < highScoreList.length; i++){
    				String[] record = Constants.getHighScoreRecord(highScoreList[i]);
    				String recordName = record[0];
    				String recordScore = record[1];
    				if(Integer.parseInt(score)>=Integer.parseInt(recordScore)){
    					isSmallest = false;
    					result = Constants.appendHighScores(result,Constants.makeHighScoreRecord(name, score)); 
    				}
    				result  = Constants.appendHighScores(result, Constants.makeHighScoreRecord(recordName, recordScore));
    			}
    			if(isSmallest) {
    				result = Constants.appendHighScores(result, Constants.makeHighScoreRecord(name, score));
    			}*/
    			int index=0;
    	        for(int i = 0; i < highScoreList.length; i++){
    	        	String record[] = Constants.getHighScoreRecord(highScoreList[i]);
    	        	if(Integer.parseInt(record[1])<=Integer.parseInt(score)&& ifSmallest){
    	        		
    	        		index=i;
    	        		
    	        		ifSmallest=false;
    	        		break;
    	        	}
    	        }
    	        
    	        for(int i = 0; i < highScoreList.length; i++){
    	        	String[] record=Constants.getHighScoreRecord(highScoreList[i]);
    	        	String recordName = record[0];
    	        	String recordScore = record[1];
    	        	if(i==index && !ifSmallest) {
    	        		if(!result.equals("")){
    	        			result = Constants.appendHighScores(result, Constants.makeHighScoreRecord(name,score));
    	        		} else {
    	        			result = Constants.makeHighScoreRecord(name,score);
    	        		}
    	        	}
    	        	if(!result.equals("")){
    	        		result = Constants.appendHighScores(result, Constants.makeHighScoreRecord(recordName, recordScore));
    	        	} else {
    	        		result = Constants.makeHighScoreRecord(recordName, recordScore);
    	        	}

    	        }
    	        if(ifSmallest){
    	        	result = Constants.appendHighScores(result, Constants.makeHighScoreRecord(name,score));
    	        }

    		}
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, Constants.HIGHSCORE,result);
    		Log.d(TAG,"KeyValueAPI put "+name+" "+score+ " result: "+result);
    		return result;
    	}
    	


    }
	/*
    void addHighScore( String playerName,String playerScore){
        HashMap<String, String> map = new HashMap<String, String>();  
        map.put(Constants.PLAYERNAMEKEY, playerName);  
        map.put(Constants.PLAYERSCOREKEY, playerScore);  
        boolean ifAdded = false;
        for(int i = 0; i < listItem.size(); i++){
        	HashMap<String,String> record = listItem.get(i);
        	if(Integer.parseInt(record.get(Constants.PLAYERSCOREKEY))<=Integer.parseInt(playerScore)&&!ifAdded){
        		
        		listItem.add(listItem.indexOf(record),map);
        		i++;
        		ifAdded=true;
        	}
        }
        if(!ifAdded){
        	listItem.add(map);  
        }
        listItemAdapter.notifyDataSetChanged();
    	
    }
    */
	void asyncGetHighScore(){
		AsyncTask<String, String,String> getHighScoreTask = new AsyncGetHighScore();
		getHighScoreTask.execute(Constants.HIGHSCORE);


	}
    public class AsyncGetHighScore extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		
    		String result = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.HIGHSCORE);
    		Log.d(TAG,"KeyValueAPI get "+result);
    		return result;
    	}
    	
    	protected void  onPostExecute(String result){
    		
    		
    		
    		try{
    			
    			if(result.equals("Error: No Such Key")){
    				listItem.clear();
    			} else {
    				Log.d(TAG,"KeyValueAPI get "+result);
    				String[] resultRecord = Constants.getHighScoreList(result);
    				for(int i = 0; i < resultRecord.length; i++){
    					String[] record = Constants.getHighScoreRecord(resultRecord[i]);
    			        HashMap<String, String> map = new HashMap<String, String>();
    			        map.put(Constants.PLAYERRANKKEY, (i+1)+"");  
    			        map.put(Constants.PLAYERNAMEKEY, record[0]);  
    			        map.put(Constants.PLAYERSCOREKEY, record[1]);  
    			        listItem.add(map);  
    			        Log.d(TAG, "get highscore record "+record[0]+" "+record[1]);
    				}
    		
    				listItemAdapter.notifyDataSetChanged();
    			}
    		} catch (Exception e){
    			Toast.makeText(getApplicationContext(), "FAIL TO GET HIGHSCORE", Toast.LENGTH_SHORT).show();
    		}
    	}

    }
    

    
    public class AsynClearKey extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {

    		String result = KeyValueAPI.clearKey(Constants.TEAMNAME, Constants.PASSWORD, Constants.HIGHSCORE);
    		Log.d(TAG,"KeyValueAPI Clear "+arg0[0]);
    		return result;
    	}
    	


    }

}
