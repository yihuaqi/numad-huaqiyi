
package edu.neu.madcourse.huaqiyi.dabbletest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.Dabble;
import edu.neu.madcourse.huaqiyi.dabbletest.TestMainMenu.ButtonListener;
import edu.neu.madcourse.huaqiyi.dictionary.Dictionary;
import edu.neu.madcourse.huaqiyi.sudoku.Sudoku;
import edu.neu.mobileClass.*;
import edu.neu.mhealth.api.*;



public class MultiFindGame extends Activity{

	Button backButton;
	Button playButton;
	TextView gameNameText;
	SimpleAdapter listItemAdapter;
	ListView list;
	String TAG = "Dabble Multi FindGame";

			
	ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();  
    @Override
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.dabble_test_multi_findgame);  

        list = (ListView) findViewById(R.id.dabble_test_multi_findgame_ListView);  
        backButton = (Button) findViewById(R.id.dabble_test_multi_findgame_BackButton);
        playButton = (Button) findViewById(R.id.dabble_test_multi_findgame_PlayButton);
        gameNameText = (TextView) findViewById(R.id.dabble_test_multi_findgame_GameName);
        ButtonListener bl = new ButtonListener();
        backButton.setOnClickListener(bl);
        playButton.setOnClickListener(bl);

        listItemAdapter = new SimpleAdapter(this, 
                                                    listItem,
                                                    R.layout.dabble_test_multi_findgamelistitem,
                                                    new String[] {Constants.OPPONENTNAMEKEY, Constants.GAMEIDKEY},   
                                                    new int[] {R.id.dabble_test_multi_findgame_listItem_PlayerName,R.id.dabble_test_multi_findgame_listItem_GameName});  

        list.setAdapter(listItemAdapter);
		list.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3){
				Log.d(TAG, "CLICK ON "+arg3);
				String opponentName ="";
				String[] record=Constants.getPlayerOpponent(listItem.get((int)arg3).get(Constants.OPPONENTNAMEKEY));
				
				if(record[0].equals(Constants.getPlayer())){
					Constants.setOpponent(record[1]);
				} else {
					Constants.setOpponent(record[0]);
				}
				
				Constants.setGameId(listItem.get((int)arg3).get(Constants.GAMEIDKEY));
				gameNameText.setText(Constants.OPPONENT+" : "+Constants.getGameId());
				//Constants.setOpponent(listItem.get((int)arg3).get(Constants.PLAYERNAMEKEY));
				//opponentName.setText(Constants.getOpponent());
			}
		});
		asyncGetActiveGame(Constants.getPlayer());
    }  

    class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View button) {

			Intent i = new Intent();
			int id = button.getId();
			if (id == R.id.dabble_test_multi_findgame_BackButton) {
				finish();
			} else if (id == R.id.dabble_test_multi_findgame_PlayButton) {
				Log.d(TAG, "Play- Player:"+Constants.getPlayer()+" Opponent:"+Constants.getOpponent()+" GameId"+Constants.getGameId());
				i.setClass(MultiFindGame.this, MultiGame.class);
				i.putExtra(Constants.HAS_BOARD, true);
				startActivity(i);
			} else {
			}
		}
    }

	


	void asyncGetActiveGame(String playerName){
		AsyncTask<String, String,String> getCreateFindGameTask = new AsyncGetActiveGame();
		getCreateFindGameTask.execute(playerName);


	}
    public class AsyncGetActiveGame extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String result = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, Constants.queryPlayerGameList(arg0[0]));
    		Log.d(TAG,"GET "+Constants.queryPlayerGameList(arg0[0])+" "+result);
    		return result;
    	}
    	
    	protected void  onPostExecute(String result){
    		listItem.clear();
			if(result.equals(Constants.EXCEPTIONNOSUCHKEY)){
				
			} else {
	    		String[] gameList = Constants.getGameList(result);
	    		for(int i = 0 ; i < gameList.length; i++){
	    			String gameName = gameList[i];
	    			String[] gameAttribute = gameName.split(Constants.ATTRIBUTESEPERATOR);
			        HashMap<String, String> map = new HashMap<String, String>();  
			        map.put(Constants.OPPONENTNAMEKEY, gameAttribute[0]+" vs "+gameAttribute[1]);  
			        map.put(Constants.GAMEIDKEY, gameName);  
			        listItem.add(map);  
	    		}	
				listItemAdapter.notifyDataSetChanged();
			}
		}
    }



}
