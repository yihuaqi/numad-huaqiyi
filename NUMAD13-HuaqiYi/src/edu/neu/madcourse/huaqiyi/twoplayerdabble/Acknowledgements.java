package edu.neu.madcourse.huaqiyi.twoplayerdabble;

import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.R.layout;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class Acknowledgements extends Activity {
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      setContentView(R.layout.dabble_acknowledgements);
      TextView ack = (TextView) findViewById(R.id.about_content);
      ack.setText("Acknowledgements\n Sound effects are all come from RPG MAKER VX.\n Microphone icon comes from http://www.iconpng.com/icon/27802 icon");
      ack.setTextSize(20);
   }
}
