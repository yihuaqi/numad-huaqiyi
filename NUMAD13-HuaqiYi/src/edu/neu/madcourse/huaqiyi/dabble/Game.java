package edu.neu.madcourse.huaqiyi.dabble;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import edu.neu.madcourse.huaqiyi.R;
import edu.neu.madcourse.huaqiyi.dabble.BoardView.Letter;
import edu.neu.madcourse.huaqiyi.sudoku.PuzzleView;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class Game extends Activity{
	   private static final String TAG = "Dabble";
	   private BoardView boardView;
	   private ArrayList<Letter> letters = new ArrayList<Letter>();
	   public String solution;
	   public String boardWord;
	   private final int numOfWords[] = {1014,4029,8937,15787};
	   public SoundPool soundPool;
	   public int DABBLE_SELECT;
	   public int DABBLE_VALID;
	   public int DABBLE_ALERT;
	   public int DABBLE_WIN;
	   public int DABBLE_LOSE;
	   static int numRoot[] = new int[]{45,87,133,182};
	   
	   protected void onCreate(Bundle savedInstanceState) {
		      super.onCreate(savedInstanceState);
		      Log.d(TAG, "Game onCreate");
		      requestWindowFeature(Window.FEATURE_NO_TITLE);
		      getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		      WindowManager.LayoutParams.FLAG_FULLSCREEN);
		      setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		      iniGame();
		      boardView = new BoardView(this);
		      soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC,5);
		      //TODO find music
		      DABBLE_SELECT = soundPool.load(this,R.raw.dabble_select, 1);
		      DABBLE_VALID = soundPool.load(this,R.raw.dabble_valid, 1);
		      DABBLE_ALERT = soundPool.load(this,R.raw.dabble_alert, 1);
		      DABBLE_WIN = soundPool.load(this,R.raw.dabble_win, 1);
		      DABBLE_LOSE = soundPool.load(this,R.raw.dabble_lose, 1);
		      
		      setContentView(boardView);
		      boardView.requestFocus();
		      
		      // ...
		      // If the activity is restarted, do a continue next time
		      //getIntent().putExtra(KEY_DIFFICULTY, DIFFICULTY_CONTINUE);
		      

		      

		   }
	   @Override
	   public boolean onCreateOptionsMenu(Menu menu) {
	      super.onCreateOptionsMenu(menu);
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.dabble_menu, menu);
	      return true;
	   }

	   @Override
	   public boolean onOptionsItemSelected(MenuItem item) {
	      int itemId = item.getItemId();
		if (itemId == R.id.dabble_settings) {
			startActivity(new Intent(this, Prefs.class));
			return true;
	      // More items go here (if any) ...
		}
	      return false;
	   }
	   protected void onResume(){
		   super.onResume();
		   //TODO add music file.
		   Music.play(this, R.raw.dabble_background);
	   }
	   protected void onPause(){
		   super.onPause();
		   if(boardView.getGameState()==boardView.GAME_PLAY){
			   boardView.setGameState(boardView.GAME_PAUSE);
		   }
		   soundPool.stop(boardView.alertStreamID);
		   boardView.allowToPlayAlert = false;
		   boardView.isPlayingAlert =false;
		   
		  
		   Music.stop(this);
				   
	   }
	   
		private String randomizeWord(String solution) {
			List<String> ls = new ArrayList<String>();
			for(int i = 0; i < solution.length(); i++){
				ls.add(String.valueOf(solution.charAt(i)));
			}
			Collections.shuffle(ls);
			StringBuffer sb = new StringBuffer();
			for(Iterator<String> it = ls.iterator(); it.hasNext();){
				sb.append(it.next());
			}
			String result = new String(sb);
			return result;
			
		}

	String getWord(int length){
		Random r= new Random(System.currentTimeMillis());
		int fileIndex = r.nextInt(numRoot[length-3]);
		int wordIndex = r.nextInt(numRoot[length-3]);
		String filename =length+"_"+fileIndex;

		int count = r.nextInt(wordIndex);
		String result="ERROR";
		try{
    		InputStream is = getAssets().open(filename);
    		InputStreamReader isReader = new InputStreamReader(is);
    		BufferedReader bReader = new BufferedReader(isReader);
    		result = null;
    		for(int i =0; i < count - 1; i++){
    			bReader.readLine();
    		}
    		result = bReader.readLine();
    		
    		
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
		return result;
	   }
	   
	   boolean inDictionary(String word){
	    	boolean result = false;
	    	String filename = word.length()+"/" +word.substring(0,2);
	    	
	    	try{
	    		InputStream is = getAssets().open(filename);
	    		InputStreamReader isReader = new InputStreamReader(is);
	    		BufferedReader bReader = new BufferedReader(isReader);
	    		String temp = null;
	    		while((temp = bReader.readLine())!=null){
	    			if(temp.equals(word)){
	    				return true;
	    			}
	    		}
	    		
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	return result;
	    }
	   void iniGame(){
		   Log.d(TAG,"iniGame");
		   letters.clear();
		   solution = getWord(3)+getWord(4)+getWord(5)+getWord(6);
		   Log.d(TAG,"Get Solution:" + solution);
		   boardWord = randomizeWord(solution);
		   Log.d(TAG,"Get boardWord:"+boardWord);

		
	   }
	   
	   String getSolution(){
		   return solution;
	   }
	   
	   String getBoardWord(){
		   return boardWord;
	   }
		ArrayList<Letter> getLetters(){
			return letters;
		}

		public void Quit() {
			finish();
			// TODO Auto-generated method stub
			
		}



	   
}
