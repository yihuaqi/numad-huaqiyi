package edu.neu.madcourse.huaqiyi.twoplayerdabble;

import java.util.HashMap;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.R.id;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;


import edu.neu.mhealth.api.KeyValueAPI;
import edu.neu.mobileClass.*;

public class NotificationTest {
	String TAG = "TWO PLAYER NOTIFICATION";
	
    public void asyncSendMessage(String sender,String receiver,String message,String gameId) {
		AsyncTask<String,String,String> addMessageTask = new AsyncSendMessage();
		
		addMessageTask.execute(sender,receiver,message,gameId);
		
	}

	

    public class AsyncSendMessage extends AsyncTask<String, String,String>{

    	@Override
    	protected String doInBackground(String... arg0) {
    		String sender = arg0[0];
    		String receiver = arg0[1];
    		String message = arg0[2];
    		String gameId = arg0[3];
    		Log.d(TAG, "AsyncSendMessage: gameId="+gameId);
    		message = Constants.makeMessage(message,gameId);
    		String queryNID = Constants.queryNotificationId(receiver);
    		String NID = Constants.makeNotificationId(sender);
    		//Constants.queryNotificationId(receiver);
    		//Constants.makeNotificationId(sender);
    		//Constants.getNotification(nID,m);
    		String notifications = KeyValueAPI.get(Constants.TEAMNAME, Constants.PASSWORD, queryNID);
    		//Constants.appendNotification(notifications,Constants.makeNotificationId(sender));
    		//Constants.getNotificationList(notifications);
    		if(notifications.equals(Constants.EXCEPTIONNOSUCHKEY)||notifications.equals(Constants.EMPTYRECORD)){
    			KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, queryNID,NID);
    			Log.d(TAG, " PUT "+NID+" by key "+queryNID);
    			
    		} else{
    			notifications = Constants.appendNotification(notifications,NID);
    			KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD,queryNID,notifications);
    			Log.d(TAG, " PUT "+notifications+" by key "+queryNID);
    		}
    		KeyValueAPI.put(Constants.TEAMNAME, Constants.PASSWORD, NID,message);		
    		Log.d(TAG, " PUT "+message+" by key "+NID);
    		
    		return null;
    	}
    
    }
    }
